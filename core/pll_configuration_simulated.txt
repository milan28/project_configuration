{ "Configuration-file": {
    "module-configuration":{
        "engineConfigurationFile": "engine/engine_configuration_simulated.txt",
        "taskConfigurationFile": "task/simulating_data/simulation_configuration.txt",
        "inferenceModelConfigurationFile": "inference_model/rim/rim_configuration.txt",
        "allowCMDoverride": true,
        "displayConfigurations": true
    },
    "task-configuration":{
        "task": "relaxometry",
        "inferenceModel": "rim"
    }
}
}