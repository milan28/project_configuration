import torch
import torch.fft
import numpy as np
import datetime
import matplotlib.pyplot as plt
from project_configuration.utilities.subsample import create_mask_for_mask_type
import project_configuration.utilities.fourier_transforms as fft


class Signal_model():
    def __init__(self, configObject):

        self.args = configObject.args

    def generate_training_signal(self, full_kspace, coil_img, slices, norm_factor, training_label, sens_mask):
        self.args.task.sens_mask = sens_mask
        self.args.task.slices = slices
        if self.args.task.type == 'multicoil':
            if self.args.task.usePatches:
                img_size = coil_img[0, 0, 0, ...].shape
                self.args.task.undMask = self.create_undersampling_mask(img_size, self.args.task.kLinesMiddle,
                                                                        self.args.task.kDensity)
                k_space_patch_list = []
                w = 0
                for patch in coil_img:
                    k_space_coil_list = []
                    for coil_img in patch:
                        k_space_image_norm = fft.fft2c_new(coil_img) / norm_factor[w]
                        k_space_image_und = torch.multiply(k_space_image_norm,
                                                           self.args.task.undMask)
                        k_space_coil_list.append(k_space_image_und)
                    k_space_patch_list.append(torch.stack(k_space_coil_list))
                    w += 1
                k_space_undersampled_re_im_split = torch.stack(k_space_patch_list)

            else:
                img_size = full_kspace[0, 0, ...].shape
                self.args.task.undMask = self.create_undersampling_mask(img_size, self.args.task.kLinesMiddle,
                                                                        self.args.task.kDensity)
                k_space_undersampled = torch.multiply(full_kspace[slices], self.args.task.undMask)
                w = 0
                k_space_list = []
                for k_space_slice in k_space_undersampled:
                    k_space_undersampled_norm = k_space_slice / norm_factor[w]
                    w += 1
                    k_space_list.append(k_space_undersampled_norm)
                k_space = torch.stack(k_space_list)
                k_space_undersampled_re_im_split = torch.view_as_real(k_space)
        elif self.args.task.type == 'singlecoil':
            img_size = training_label[0, 0, ...].shape
            self.args.task.undMask = self.create_undersampling_mask(img_size, self.args.task.kLinesMiddle,
                                                                    self.args.task.kDensity)
            k_space_patch_list = []
            for patch in training_label:
                k_space_fully_sampled = fft.fft2c_new(patch)
                k_space_und = torch.multiply(k_space_fully_sampled, self.args.task.undMask)
                k_space_patch_list.append(k_space_und)
            k_space_undersampled_re_im_split = torch.stack(k_space_patch_list)

        return k_space_undersampled_re_im_split.float().to(self.args.engine.device)


    def create_undersampling_mask(self, img_size, lines_middle, k_density):
        k_space_size_x = img_size[0]
        k_space_size_y = img_size[1]
        shape = [img_size[0], img_size[1], 1]
        if self.args.task.maskType == 'original':
            k_lines_middle = lines_middle
            middle = k_space_size_x // 2
            k_density_inv = k_density ** (-1)
            # k_density_inv = torch.normal(k_density, 0.1, size=(1, 1)) ** (-1)
            mask = torch.zeros(k_space_size_x, k_space_size_y)
            sampled_lines = np.round(np.arange(0, k_space_size_x - 1, k_density_inv))
            mask[sampled_lines, :] = 1
            mask[int(middle - (k_lines_middle - 1) / 2): int(middle + (k_lines_middle) / 2), :] = 1
        if self.args.task.maskType == 'equispaced':
            acceleration = [1 / self.args.task.kDensity]
            mask_func = create_mask_for_mask_type(self.args.task.maskType, [self.args.task.kCenter_fractions],
                                                  acceleration)
            mask = mask_func(shape)
        if self.args.task.maskType == 'random':
            acceleration = [1 / self.args.task.kDensity]
            mask_func = create_mask_for_mask_type(self.args.task.maskType, [self.args.task.kCenter_fractions],
                                                  acceleration)
            if self.args.task.useRandomSeedMask:
                tim = datetime.datetime.now()
                randseed = tim.hour * 10000 + tim.minute * 100 + tim.second + tim.microsecond
                mask = mask_func(shape, randseed)
            else:
                mask = mask_func(shape, 10)
        return mask.to(self.args.engine.device)

    def initializeParameters(self, signal):  # label
        if self.args.task == 'singlecoil':
            img_for_each_patch = []
            for signal_patch in signal:
                w_image_complex_re_im_split = fft.ifft2c_new(signal_patch)
                img_for_each_patch.append(w_image_complex_re_im_split)
            label_guess = torch.stack(img_for_each_patch)
        else:
            img_list = []
            for patch in signal:
                coil_img_list = []
                for coil in patch:
                    coil_img = fft.ifft2c_new(coil)
                    coil_img_list.append(coil_img)
                img_list.append(self.rss(torch.stack(coil_img_list)))
            label_guess = torch.stack(img_list)

        return label_guess.float()




    def generateUndKspace(self, predicted_weighted_image_0, index):  # signal
        if self.args.task.type == 'singlecoil':
            k_space = fft.fft2c_new(predicted_weighted_image_0)
            k_space_coils_undersampled = torch.view_as_real(torch.multiply(torch.view_as_complex(k_space), self.args.task.undMask))
        elif self.args.task.type == 'multicoil':
            k_space_coil_list = []
            sens_masks_slices = self.args.task.sens_mask[self.args.task.slices[index]]
            for sen_mask in sens_masks_slices:
                img_sens = torch.view_as_real(torch.view_as_complex(predicted_weighted_image_0) * sen_mask)
                k_space_sens = fft.fft2c_new(img_sens)
                k_space_undersampled = torch.view_as_real(torch.multiply(torch.view_as_complex(k_space_sens), self.args.task.undMask))
                k_space_coil_list.append(k_space_undersampled)
            k_space_coils_undersampled = torch.stack(k_space_coil_list)
        return k_space_coils_undersampled.float()


    def rss(self, img):
        return torch.sqrt((img ** 2).sum(0))
