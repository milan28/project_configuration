import h5py
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import torch
import torch.nn.functional as F
from math import prod
from EMCqMRI.core.utilities import image_utilities
from skimage.metrics import structural_similarity as ssim2


def nmse(label, estimated):
    return np.sum(((estimated - label) ** 2)) / np.sum(label ** 2)


def ssim(estimated, label):
    estimated = torch.unsqueeze(torch.from_numpy(estimated), (0, 1))
    label = torch.unsqueeze(torch.from_numpy(label), (0, 1))
    data_range = abs(label).max()
    k1 = 0.01
    k2 = 0.03
    win_size = 7
    NP = win_size ** 2
    cov_norm = NP / (NP - 1)
    w = torch.ones(win_size, win_size) / win_size ** 2

    C1 = (k1 * data_range) ** 2
    C2 = (k2 * data_range) ** 2
    ux = F.conv2d(estimated, w)  # typing: ignore
    uy = F.conv2d(label, w)  #
    uxx = F.conv2d(estimated * estimated, w)
    uyy = F.conv2d(label * label, w)
    uxy = F.conv2d(estimated * label, w)
    vx = cov_norm * (uxx - ux * ux)
    vy = cov_norm * (uyy - uy * uy)
    vxy = cov_norm * (uxy - ux * uy)
    A1, A2, B1, B2 = (
        2 * ux * uy + C1,
        2 * vxy + C2,
        ux ** 2 + uy ** 2 + C1,
        vx + vy + C2,
    )
    D = B1 * B2
    S = (A1 * A2) / D

    return 1 - S.mean()


def psnr(estimated, label):
    MSE = ((estimated - label) ** 2).mean(axis=None)
    return 10 * np.log10((abs(label).max() ** 2) / MSE)


result = h5py.File('data/generated/training/v01subject06_crisp_v_epoch_estimating_0.h5', 'r')
RIM = result['rimtesting']
# result = h5py.File('data/generated/estimated_data/v85subject54_crisp_v_epoch680.h5', 'r')
# RIM = result['rimtraining']
patch = 0

# result = h5py.File('sens_maps.h5')

useMask = True
mask = RIM['mask'][patch]
mask3 = np.stack((mask, mask, mask), 0)
estimated_real_imag = np.moveaxis(RIM['estimated'], -1, 0)
estimated_abs = np.expand_dims((np.sqrt(estimated_real_imag[0] ** 2 + estimated_real_imag[1] ** 2)), 0)
estimated = np.concatenate((estimated_real_imag, estimated_abs), 0)
label_real_imag = np.moveaxis(RIM['labels'][patch], -1, 0)
label_abs = np.expand_dims((np.sqrt(label_real_imag[0] ** 2 + label_real_imag[1] ** 2)), 0)
label = np.concatenate((label_real_imag, label_abs), 0)
initial_guess_real_imag = np.moveaxis(RIM['initial'][patch], -1, 0)
initial_guess_abs = np.expand_dims((np.sqrt(initial_guess_real_imag[0] ** 2 + initial_guess_real_imag[1] ** 2)), 0)
initial_guess = np.concatenate((initial_guess_real_imag, initial_guess_abs), 0)
if useMask:
    initial_guess[mask3 == 0] = 0
    estimated[mask3 == 0] = 0
keys = ['Real', 'Imaginary', 'Magnitude', "Aliased image"]
cmap = plt.get_cmap('PuOr')
diff = estimated - label

cmap = plt.get_cmap('PuOr')
# norm = mpl.colors.Normalize(vmin=diff.min(), vmax=diff.max())
table = np.ones((3, 4))
rows_table = ['NMSE', 'PSNR', 'SSIM']
for i in range(1):
    i=2
    NMSE = nmse(label[i], estimated[i])
    # SSIM = ssim(label[i], estimated[i])
    PSNR = psnr(label[i], estimated[i])
    SSIM = ssim2(label[i], estimated[i], data_range=label[i].max(), win_size=7)
    table[0, i] = NMSE
    table[1, i] = PSNR
    table[2, i] = SSIM
    print("NMSE of " + keys[i] + " " + str(NMSE))
    print("PSNR of " + keys[i] + " " + str(PSNR))
    print("SSIM of " + keys[i] + " " + str(SSIM))
    if i == 2:
        SSIM_init = ssim2(label[i], initial_guess[i], data_range=label[i].max(), win_size=7)
        NMSE_init = nmse(label[i], initial_guess[i])
        PSNR_init = psnr(label[i], initial_guess[i])
        table[0, 3] = NMSE_init
        table[1, 3] = PSNR_init
        table[2, 3] = SSIM_init

    est_flat = estimated[i].flatten()
    label_flat = label[i].flatten()
    vmax = max([estimated[i].max(), label[i].max()])
    vmin = min([estimated[i].min(), label[i].min()])
    v_diff = abs(diff[i]).max()
    fig1, axs = plt.subplots(3, 2, figsize=(20, 20))
    fig1.suptitle(keys[i])
    im1 = axs[0, 0].imshow(label[i], cmap='gray', vmax=vmax, vmin=vmin)
    fig1.colorbar(im1, ax=axs[0, 0])
    axs[0, 0].set_title("Ground Truth")
    im2 = axs[0, 1].imshow(estimated[i], cmap='gray', vmax=vmax, vmin=vmin)
    fig1.colorbar(im2, ax=axs[0, 1])
    axs[0, 1].set_title("Inferred image")
    im3 = axs[1, 0].imshow(diff[i], cmap=cmap, vmax=v_diff, vmin=-v_diff)
    fig1.colorbar(im3, ax=axs[1, 0])
    axs[1, 0].set_title("Difference")
    axs[1, 1].scatter(est_flat, label_flat, marker=".", alpha=0.1)
    axs[1, 1].plot([-3, 3], [-3, 3], color='black')
    axs[1, 1].set_xlabel('Estimated')
    axs[1, 1].set_xlim(min(est_flat), max(est_flat))
    axs[1, 1].set_ylim(min(est_flat), max(est_flat))
    axs[1, 1].set_ylabel("Inferred")
    axs[1, 1].set_title("Scatter plot")
    im4 = axs[2, 0].imshow(initial_guess[i], cmap='gray', vmax=vmax, vmin=vmin)
    fig1.colorbar(im4, ax=axs[2, 0])
    axs[2, 0].set_title("Aliased Image")
    fig1.delaxes(axs[2][1])
    plt.show()
table = np.around(table, 5)
the_table = plt.table(cellText=table,
                      rowLabels=rows_table,
                      colLabels=keys,
                      loc='center')
ax = plt.gca()
ax.get_xaxis().set_visible(False)
ax.get_yaxis().set_visible(False)
the_table.auto_set_font_size(False)
the_table.set_fontsize(12)
plt.box(on=None)
the_table.scale(0.8, 2)
plt.show()
