import numpy as np
import h5py
import matplotlib.pyplot as plt

par = h5py.File('scanning_parameters.h5', 'r')
TE = np.round(np.expand_dims(par['TE'][:], 0), 1)
TR = np.round(np.expand_dims(par['TR'][:], 0), 0)
table = np.concatenate((TE, TR), axis=0)
uni, count = np.unique(table, axis=1, return_counts=True)
per = np.round(np.expand_dims(count/90*100, 0),1)
table_2 = np.concatenate((uni, per), 0)
rows = ['Echo time (ms)', 'Repetition time (ms)', 'Frequency (%)']
the_table = plt.table(cellText=np.transpose(table_2),
                      colLabels=rows,
                      loc='center')
ax = plt.gca()
ax.get_xaxis().set_visible(False)
ax.get_yaxis().set_visible(False)
the_table.auto_set_font_size(False)
the_table.set_fontsize(11)
plt.box(on=None)
the_table.scale(1, 1.5)
# plt.tight_layout()
plt.show()