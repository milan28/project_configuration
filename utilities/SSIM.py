import torch
# import project_configuration.utilities.pytorch_ssim.pytorch_ssim as pytorch_ssim
from pytorch_msssim import ssim as ssim1
# from piqa.ssim import ssim
# from piqa.utils.functional import gaussian_kernel

def SSIM(estimated, label):
    estimated_abs = torch.unsqueeze(abs(torch.view_as_complex(estimated)), 0)
    label_abs = torch.unsqueeze(abs(torch.view_as_complex(label)), 0)
    # ssim_loss = pytorch_ssim.SSIM(window_size=7)
    # loss2 = 1- ssim_loss(estimated_abs, label_abs, data_range=label_abs.max())
    loss = 1 - ssim1(estimated_abs, label_abs, data_range=label_abs.max(), win_size = 7)
    # kernel = gaussian_kernel(7, sigma=1).repeat(2, 1, 1)
    # loss = ssim(estimated_abs, label_abs, kernel=kernel, channel_avg=True, value_range=label_abs.max())
    return loss
