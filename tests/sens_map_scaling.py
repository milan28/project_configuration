import numpy as np
from scipy import ndimage, misc
import h5py
import os
from project_configuration.utilities import transforms
import matplotlib.pyplot as plt
from project_configuration.utilities.plotter import plotter

def load_folder(fileName, path, crop_size, desired_size):
    data = []
    sens_map_list =[]
    all_sens_maps = []
    for file in fileName:
        with open(os.path.join(path, file), 'rb') as f:
            data_ = h5py.File(f, 'r')
            sens_maps = np.moveaxis(data_['sens_maps'][:], -1, 1)
            if sens_maps.shape[1] == 20:
                print('stop')
            if sens_maps.shape[1] == 16:
                sens_maps_cropped = transforms.center_crop(sens_maps, crop_size)
                scaling_factor = [desired_size[0]/crop_size[0], desired_size[1]/crop_size[1]]
                coil_list = []
                for coil in sens_maps_cropped[0]:
                    coil_real = coil.real
                    coil_imag = coil.imag
                    map_scaled_real = ndimage.zoom(coil_real, scaling_factor, order=3)
                    map_scaled_imag = ndimage.zoom(coil_imag, scaling_factor, order=3)
                    coil_list.append(map_scaled_real+map_scaled_imag*1j)
            sens_map_list.append(np.stack(coil_list))
    all_sens_maps = np.stack(sens_map_list)

    h5 = h5py.File(os.path.join(path, 'simulated/simulated_sens_mapsv2.h5'), 'w')
    h5.create_dataset("sens_maps", data=all_sens_maps)
    h5.close()
    return data


def index_files(path, crop_size, desired_size):
    extension = ".h5"
    fileName = sorted(
        [f for f in os.listdir(path) if (os.path.isfile(os.path.join(path, f)) and (extension in f))])
    data = load_folder(fileName, path, crop_size, desired_size)
    return data


path = "D:/sens_map_training/simulated"

crop_size = (250,280)
desired_size = (434, 362)
data = index_files(path, crop_size, desired_size)