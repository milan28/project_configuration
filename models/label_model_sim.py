from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys

sys.path.insert(0, "../")

import EMCqMRI.core.utilities.dataset_utilities as datasetUtilities
import numpy as np
import torch
import math
import matplotlib.pyplot as plt
from project_configuration.utilities.plotter import plotter


class Label_model():
    def __init__(self, args, data, sensitivityMap):
        super(Label_model, self).__init__()
        self.data = data
        self.args = args
        self.sensitivityMap = sensitivityMap
        self.TI = torch.div(torch.Tensor(args.task.TI), 1000)
        self.TE = torch.div(args.task.TE, 1000)
        self.TR = torch.div(torch.Tensor(args.task.TR), 1000)

    def generate_simulated_training_label(self, idx):
        data_ = self.data[idx]
        self.args.task.pngr = datasetUtilities.get_rand_seed(self.args.task.useRandomSeed)
        if self.args.task.usePatches:
            patch_extractor = datasetUtilities.ExtractPatch(self.args)
            data_ = patch_extractor.get_patch(data_)
            slice_index = int(self.args.task.pngr.uniform(10, int(data_.shape[-1]) - 10))
            data_ = data_[..., slice_index]
        else:
            data_, slice_index = self.slice_extractor(data_)

        T1Map = np.zeros_like(data_, dtype=float)
        T2Map = np.zeros_like(data_, dtype=float)
        pdMap = np.zeros_like(data_, dtype=float)
        mask = np.zeros_like(data_, dtype=float)
        mask[data_ > 0] = 1

        self.set_parameter_values()
        T1_param = list(self.parameters[0].items())
        T2_param = list(self.parameters[1].items())
        proton_density = list(self.parameters[2].items())
        std_tissue_PD = [0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0, 0.3, 0.3, 0.3, 0.3]
        # std_tissue_T1 = [0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01]
        # std_tissue_T2 = [0., 0.01, 0.01, 0.01, 0.01, 0.01, 0.05, 0.01, 0.01, 0.01, 0.01]
        std_tissue_T1 = [0.3, 0.3, 0.25, 0.1, 0.3, 0.3, 0.1, 0.3, 0.25, 0.25, 0.1]
        std_tissue_T2 = [0.3, 0.03, 0.02, 0.02, 0.02, 0.02, 0.01, 0.07, 0.02, 0.02, 0.02]
        # std_tissue_PD = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        # std_tissue_T1 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        # std_tissue_T2 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        T1_noise = np.zeros(((len(T1_param),) + T1Map[0].shape), dtype=float)
        T2_noise = np.zeros_like(T1_noise, dtype=float)
        PD_noise = np.zeros_like(T1_noise, dtype=float)
        for ind in np.unique(data_):
            if ind > 0:
                T1_noise[ind - 1] = np.random.normal(0, T1_param[ind - 1][1] / self.args.task.tissueNoise,
                                                     np.shape(T1Map[0]))
                T2_noise[ind - 1] = np.random.normal(0, T2_param[ind - 1][1] / self.args.task.tissueNoise,
                                                     np.shape(T1Map[0]))
                PD_noise[ind - 1] = np.random.normal(0, proton_density[ind - 1][1] / self.args.task.tissueNoise,
                                                     np.shape(T1Map[0]))

        if len(np.shape(data_)) > 2:
            self.args.task.pngr = datasetUtilities.get_rand_seed(self.args.task.useRandomSeed)
            for p in range(len(data_)):
                for ind in np.unique(data_):
                    if ind > 0:
                        T1_val = np.abs(self.args.task.pngr.normal(T1_param[ind - 1][1], std_tissue_T1[ind - 1]))
                        T2_val = np.abs(self.args.task.pngr.normal(T2_param[ind - 1][1], std_tissue_T2[ind - 1]))
                        PD_val = np.abs(self.args.task.pngr.normal(proton_density[ind - 1][1], std_tissue_PD[ind - 1]))
                        T1Map[p, data_[p] == ind] = T1_val + T1_noise[ind - 1, data_[p] == ind]
                        T2Map[p, data_[p] == ind] = T2_val + T2_noise[ind - 1, data_[p] == ind]
                        pdMap[p, data_[p] == ind] = PD_val + PD_noise[ind - 1, data_[p] == ind]
        else:
            for ind in np.unique(data_):
                if ind > 0:
                    T1Map[data_ == ind] = np.abs(self.args.task.pngr.normal(T1_param[ind - 1][1], 0.3))
                    T2Map[data_ == ind] = np.abs(self.args.task.pngr.normal(T2_param[ind - 1][1], 0.3))
                    pdMap[data_ == ind] = np.abs(self.args.task.pngr.normal(proton_density[ind - 1][1], 0.3))

        bMap = np.ones(np.shape(T1Map)) * 2
        kappa = np.stack([pdMap, bMap, T1Map, T2Map])

        if self.args.task.simulateArtefacts:
            kappa = datasetUtilities.add_artefacts(kappa)

        kappa = datasetUtilities.smooth_maps(kappa, self.args)

        parameter_label = torch.from_numpy(np.abs(kappa)).float()
        mask = torch.from_numpy(mask).float()
        mask = (mask[:,0] == 1) | (mask[:,1] == 1) | (mask[:,2] == 1)

        weightedSeries = []
        for parameter_patch, mask_ in zip(parameter_label, mask):
            weighted_images = self.generateWeightedImages(parameter_patch, mask_)
            weightedSeries.append(weighted_images)
        training_label = torch.stack(weightedSeries).type(torch.FloatTensor)
        if self.args.task.type == 'multicoil':
            training_label_multicoil, training_label_gt, sens_map_slices = self.convert_to_multicoil(training_label)
            return training_label_gt.float().to(
                self.args.engine.device), mask, training_label_multicoil, sens_map_slices
        else:
            return training_label, mask

    def set_parameter_values(self):
        if self.args.task.weighing == 'T1':
            T_1 = {"csf": 3.5, "gm": 1.4, "wm": 0.78, "fat": 0.42,
                   "muscle": 1.2, "muscle_skin": 1.23, "skull": 0, "vessels": 1.98,
                   "connect": 0.9, "dura": 0.9, "bone_marrow": 0.58}
            pd = {"csf": 1.0, "gm": 0.85, "wm": 0.65, "fat": 0.9,
                  "muscle": 0.7, "muscle_skin": 0.7, "skull": 0, "vessels": 1.0,
                  "connect": 0.7, "dura": 0.7, "bone_marrow": 0.8}
            T_2 = {"csf": 2, "gm": 0.11, "wm": 0.08, "fat": 0.07,
                   "muscle": 0.05, "muscle_skin": 0.05, "skull": 0.0, "vessels": 0.275,
                   "connect": 0.08, "dura": 0.07, "bone_marrow": 0.05}
            T_1 = {"csf": 2.569, "gm": .833, "wm": 0.5, "fat": 0.35,
                   "muscle": 0.9, "muscle_skin": 2.569, "skull": 0, "vessels": 0.833,
                   "connect": 0.9, "dura": 0.9, "bone_marrow": 0.58}
            pd = {"csf": 1.0, "gm": 0.85, "wm": 0.65, "fat": 0.9,
                  "muscle": 0.7, "muscle_skin": 0.7, "skull": 0, "vessels": 1.0,
                  "connect": 0.7, "dura": 0.7, "bone_marrow": 0.8}
            T_2 = {"csf": 2, "gm": 0.11, "wm": 0.08, "fat": 0.07,
                   "muscle": 0.05, "muscle_skin": 0.05, "skull": 0.0, "vessels": 0.275,
                   "connect": 0.08, "dura": 0.07, "bone_marrow": 0.05}
        self.parameters = [T_1, T_2, pd]

    def forwardModel(self, TI, TE, TR, kappa):
        # term_1 = torch.exp(torch.div(-TI, torch.abs(kappa[2])))
        term_2 = torch.exp(torch.div(-TR, torch.abs(kappa[2])))
        term_3 = torch.exp(torch.div(-TE, torch.abs(kappa[3])))
        return torch.abs(torch.abs(kappa[0]) * (1 - term_2) * term_3)

    def generateWeightedImages(self, kappa, mask):
        TI = self.args.task.pngr.uniform(self.TI[0], self.TI[1])
        TE = self.TE
        TR = self.args.task.pngr.uniform(self.TR[0], self.TR[1])
        w_image = torch.sum((torch.squeeze(self.forwardModel(TI, TE, TR, kappa).view(kappa[0].shape)) * mask),
                            0) / self.args.task.sliceThickness
        w_image_complex_re_im_split = self.add_complex_channel(w_image)
        return w_image_complex_re_im_split

    def add_complex_channel(self, w_image):
        range_min = 0 * math.pi
        range_max = 2 * math.pi
        phase = self.args.task.pngr.uniform(range_min, range_max)
        real = torch.unsqueeze((math.cos(phase) * w_image), -1)
        imag = torch.unsqueeze((math.sin(phase) * w_image), -1)
        return torch.cat((real, imag), -1)

    def slice_extractor(self, img):
        img_list = []
        slices = self.args.task.pngr.choice(range(80, img.shape[0] - 80), self.args.task.numberOfPatches, replace=False)
        for slice in slices:
            img_list.append(img[slice:slice+self.args.task.sliceThickness])
        img_slice = np.stack(img_list)
        return img_slice, slices

    def convert_to_multicoil(self, training_label):
        training_label_complex = torch.view_as_complex(training_label)
        training_label_complex_multicoil_list = []
        training_label_gt_list = []
        sens_map_slice_list = []
        for patch in training_label_complex:
            sens_map_slice = self.args.task.pngr.randint(self.sensitivityMap.size()[0])
            sens_map_slice_list.append(sens_map_slice)
            coil_img_list = []
            for coil_sens in self.sensitivityMap[sens_map_slice]:
                coil_img = patch * coil_sens
                coil_img_list.append(coil_img)
            training_label_complex_multicoil = torch.stack(coil_img_list)
            ground_truth = self.create_ground_truth(training_label_complex_multicoil, sens_map_slice)
            training_label_gt_list.append(ground_truth)
            training_label_complex_multicoil_list.append(training_label_complex_multicoil)
        training_label_gt = torch.view_as_real(torch.stack(training_label_gt_list))
        training_label_multicoil = torch.view_as_real(torch.stack(training_label_complex_multicoil_list))

        return training_label_multicoil, training_label_gt, sens_map_slice_list

    def create_ground_truth(self, img_coil, sens_map_slice):
        img_size = tuple(img_coil.shape[1:3])
        sens_maps = self.sensitivityMap[sens_map_slice]
        coil_combined = torch.zeros(img_size, dtype=torch.cfloat)
        for sens_, img_ in zip(sens_maps, img_coil):
            coil_combined += img_ * torch.conj(sens_)
        return coil_combined
