import torch

def NMSE(estimated, label, timestep, T, mask):

    estimated_complex = torch.view_as_complex(estimated)
    label_complex = torch.view_as_complex(label)
    # weight = 10 **(-(T-timestep)/(T-1))
    loss = 0
    weight = 1
    for est, lab in zip(estimated_complex, label_complex):
        loss += weight * torch.sum(((abs(est) - abs(lab)) ** 2)*mask) / torch.sum(abs(lab) ** 2 * mask)

    # estimated_complex = torch.view_as_complex(estimated)
    # label_complex = torch.view_as_complex(label)
    # loss = torch.sum(((abs(estimated_complex) - abs(label_complex)) ** 2)) / torch.sum(abs(estimated_complex) ** 2)
    return loss
