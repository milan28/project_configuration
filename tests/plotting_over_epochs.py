import matplotlib.pyplot as plt

epochs = [25, 50, 100, 150, 250, 350, 450, 550, 650]
NMSE = [0.009194947, 0.010387983, 0.007717572, 0.006984179, 0.0072791837, 0.008237058, 0.007985721, 0.007797399,
        0.007555373]
PSNR = [35.63274830404983, 34.86467754206555, 36.625170983127894, 36.377962194857346, 35.96565076426772,
        36.00555104768436, 36.12649374761882, 36.494021572113915, 35.91307594151609]
SSIM = [0.8613891944315298, 0.8358033229330399, 0.8810259913880988, 0.8471806510149431, 0.8282428100574408,
        0.7898967349007937, 0.7910808919509347, 0.8227389030177427, 0.8135016597683764]
NMSE_init = [0.02066, 0.02066]
PSNR_init = [31.0908, 31.0908]
SSIM_init = [0.85423, 0.85423]
NMSE_S = [0.0127866585, 0.011532146, 0.011193952, 0.009686139, 0.0056231497, 0.0035196196, 0.0031197008, 0.0038936902,0.0037871131]
PSNR_S = [26.632096164382336, 27.672470813473197, 28.042315945697695, 28.017891973838527, 30.63663323671692,
          32.13795255494931, 32.667491851829766, 31.94847238166443, 31.975859287533822]
SSIM_S = [0.6362740250902784, 0.6408529974515762, 0.6823724616342031, 0.7541570855704565, 0.8520118752156675,
          0.9048951143914075, 0.9132040102190053, 0.8927662181136622, 0.9040787165113375]
NMSE_init_S = [0.02463, 0.02463]
SSIM_init_S = [0.56, 0.56]
PSNR_init_S = [22.35731, 22.35731]
epochs_init = [0, 650]


fig1, axs = plt.subplots(2, 2, figsize=(10, 10))
fig1.suptitle('FastMRI data inferred with RIM trained with simulated data')
plt1 = axs[0, 0].plot(epochs, NMSE, label='estimated image')
plt1 = axs[0, 0].plot(epochs_init, NMSE_init, label='aliased image')
axs[0, 0].set(xlabel='epochs')
axs[0, 0].set_title("NMSE")
axs[0, 0].legend()
plt2 = axs[0, 1].plot(epochs, PSNR, label='estimated image')
plt2 = axs[0, 1].plot(epochs_init, PSNR_init, label='aliased image')
axs[0, 1].set(xlabel='epochs')
axs[0, 1].set_title("PSNR")
axs[0, 1].legend()
plt2 = axs[1, 0].plot(epochs, SSIM, label='estimated image')
plt2 = axs[1, 0].plot(epochs_init, SSIM_init, label='aliased image')
axs[1, 0].set(xlabel='epochs')
axs[1, 0].set_title("SSIM")
axs[1, 0].legend()
plt.tight_layout()
plt.show()
fig1, axs = plt.subplots(2, 2, figsize=(10, 10))
fig1.suptitle('Simulated data inferred with RIM trained with simulated data')
plt1 = axs[0, 0].plot(epochs, NMSE_S, label='estimated image')
plt1 = axs[0, 0].plot(epochs_init, NMSE_init_S, label='aliased image')
axs[0, 0].set(xlabel='epochs')
axs[0, 0].set_title("NMSE")
axs[0, 0].legend()
plt2 = axs[0, 1].plot(epochs, PSNR_S, label='estimated image')
plt2 = axs[0, 1].plot(epochs_init, PSNR_init_S, label='aliased image')
axs[0, 1].set(xlabel='epochs')
axs[0, 1].set_title("PSNR")
axs[0, 1].legend()
plt2 = axs[1, 0].plot(epochs, SSIM_S, label='estimated image')
plt2 = axs[1, 0].plot(epochs_init, SSIM_init_S, label='aliased image')
axs[1, 0].set(xlabel='epochs')
axs[1, 0].set_title("SSIM")
axs[1, 0].legend()
plt.tight_layout()
plt.show()
