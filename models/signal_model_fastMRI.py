import torch
import torch.fft
import numpy as np
import datetime
import matplotlib.pyplot as plt
from project_configuration.utilities.subsample import create_mask_for_mask_type
import project_configuration.utilities.fourier_transforms as fft
from project_configuration.utilities.transforms import complex_center_crop
from project_configuration.utilities.transforms import center_crop
from project_configuration.utilities.plotter import plotter


class Signal_model():
    def __init__(self, configObject):
        self.args = configObject.args

    def generate_training_signal(self, full_kspace, coil_img, slices, training_label, sens_map, img_size, k_space_size, norm_factor):
        self.sens_map = sens_map
        self.sens_map_CUDA = sens_map.to(self.args.engine.device)
        self.slices = slices
        self.img_size = img_size
        self.k_space_size = k_space_size
        self.undMask = self.create_undersampling_mask(k_space_size)
        self.undMask_CUDA = self.undMask.to(self.args.engine.device)
        if self.args.task.type == 'multicoil':
            if self.args.task.usePatches:
                k_space_patch_list = []
                w = 0
                for patch in coil_img:
                    k_space_coil_list = []
                    for coil_img in patch:
                        k_space_image = fft.fft2c_new(coil_img)
                        k_space_image_und = torch.multiply(k_space_image,
                                                           self.undMask)
                        k_space_coil_list.append(k_space_image_und)
                    k_space_patch_list.append(torch.stack(k_space_coil_list))
                    w += 1
                k_space_undersampled_re_im_split = torch.stack(k_space_patch_list)
            else:
                k_space_undersampled = torch.multiply(full_kspace[slices], self.undMask)
                k_space_undersampled_norm = k_space_undersampled * norm_factor
                k_space_undersampled_re_im_split = torch.view_as_real(k_space_undersampled_norm)
        elif self.args.task.type == 'singlecoil':
            k_space_patch_list = []
            for patch in training_label:
                k_space_fully_sampled = fft.fft2c_new(patch)
                k_space_und = torch.multiply(k_space_fully_sampled, self.undMask)
                k_space_patch_list.append(k_space_und)
            k_space_undersampled_re_im_split = torch.stack(k_space_patch_list)

        return k_space_undersampled_re_im_split.float().to(self.args.engine.device)

    def create_undersampling_mask(self, img_size):
        shape = [img_size[0], img_size[1], 1]
        if isinstance(self.args.task.kDensity, list):
            acceleration = [1 / self.args.task.pngr.choice(np.array(self.args.task.kDensity))]
        else:
            acceleration = [1 / self.args.task.kDensity]
        if acceleration[0] == 4:
            middle_lines = 0.08
        elif acceleration[0] == 8:
            middle_lines = 0.04
        else:
            middle_lines = 0.08
        if self.args.task.maskType == 'equispaced':
            mask_func = create_mask_for_mask_type(self.args.task.maskType, [middle_lines],
                                                  acceleration)
            mask = mask_func(shape)
        elif self.args.task.maskType == 'random':
            mask_func = create_mask_for_mask_type(self.args.task.maskType, [middle_lines],
                                                  acceleration)
            if self.args.task.useRandomSeedMask:
                tim = datetime.datetime.now()
                randseed = tim.hour * 10000 + tim.minute * 100 + tim.second + tim.microsecond
                mask = mask_func(shape, randseed)
            else:
                mask = mask_func(shape, 10)
        return mask

    def initializeParameters(self, signal, indexes):  # label
        if self.args.task == 'singlecoil':
            img_for_each_patch = []
            for signal_patch in signal:
                w_image_complex_re_im_split = fft.ifft2c_new(signal_patch)
                img_for_each_patch.append(w_image_complex_re_im_split)
            label_guess = torch.stack(img_for_each_patch)
        else:
            gt_list = []
            all_coil_img_list = []
            for o, k_space_1 in enumerate(signal):
                sens_maps = self.sens_map_CUDA[self.slices[indexes[-1]+o]]
                coil_img_list = []
                coil_combined = torch.zeros((self.img_size), dtype=torch.cfloat).to(self.args.engine.device)
                for sens_, img_ in zip(sens_maps, k_space_1):
                    coil_img = torch.view_as_complex(fft.ifft2c_new(img_))
                    coil_img_list.append(coil_img)
                    coil_combined += center_crop(coil_img * torch.conj(sens_), self.img_size)
                gt_list.append(coil_combined)
                all_coil_img_list.append(torch.stack(coil_img_list))
            label_guess = torch.view_as_real(torch.stack(gt_list))

        return label_guess.float(), torch.view_as_real(torch.stack(all_coil_img_list)).cpu()

    def generateUndKspace(self, predicted_weighted_image_0, index):  # signal
        if self.args.task.type == 'singlecoil':
            k_space = fft.fft2c_new(predicted_weighted_image_0)
            k_space_coils_undersampled = torch.view_as_real(
                torch.multiply(torch.view_as_complex(k_space), self.undMask))
        elif self.args.task.type == 'multicoil':
            padding = ((np.asarray(self.k_space_size) - np.asarray(self.img_size))/2).astype(int)
            padder = torch.nn.ZeroPad2d((padding[1], padding[1], padding[0], padding[0]))
            predicted_weighted_image_0_padded = torch.movedim(
                padder(torch.movedim(predicted_weighted_image_0, 0, 1).contiguous()), 0, 1).contiguous()
            k_space_coil_list = []
            sens_maps_slices = self.sens_map_CUDA[self.slices[index]]
            for sens_map in sens_maps_slices:
                img_sens = torch.view_as_real(torch.view_as_complex(predicted_weighted_image_0_padded) * sens_map)
                k_space_sens = fft.fft2c_new(img_sens)
                k_space_undersampled = torch.view_as_real(
                    torch.multiply(torch.view_as_complex(k_space_sens), self.undMask_CUDA))
                k_space_coil_list.append(k_space_undersampled)
            k_space_coils_undersampled = torch.stack(k_space_coil_list)
        return k_space_coils_undersampled.float()

