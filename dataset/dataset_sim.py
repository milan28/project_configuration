from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
import EMCqMRI

sys.path.insert(0, "../")

from EMCqMRI.core.base.base_dataset import Dataset
from project_configuration.models.label_model_sim import Label_model as Label_model_sim
from project_configuration.models.label_model_fastMRI import Label_model as Label_model_fastMRI
import EMCqMRI.core.utilities.dataset_utilities as datasetUtilities
import numpy as np
import os
import pickle
import torch
import h5py
from random import randrange
import ntpath
from project_configuration.utilities import coil_combine
import matplotlib.pyplot as plt
import xml.etree.ElementTree as etree
from project_configuration.utilities.fast_mri_data import et_query



class MyDataset(Dataset):
    def __init__(self, configObject):
        self.args = configObject.args
        self.data = []
        self.idx_control = -1
        self.sensitivityMap = configObject.sensitivityMap

    def set_data_path(self, path):
        self.path = path

    def load_folder(self):
        for file in self.fileName:
            data_conv = {}
            with open(os.path.join(self.path, file), 'rb') as f:
                data_ = h5py.File(f, 'r')
                if 'kspace' in data_:
                    data_conv['kspace'] = data_['kspace'][()]
                if 'reconstruction_rss' in data_:
                    data_conv['reconstruction_rss'] = data_['reconstruction_rss'][()]
                if 'mask' in data_:
                    data_conv['undersampling_mask'] = data_['mask'][()]
                if "ismrmrd_header" in data_:
                    et_root = etree.fromstring(data_["ismrmrd_header"][()])
                    enc = ["encoding", "reconSpace", "matrixSize"]
                    crop_size = (
                        int(et_query(et_root, enc + ["x"])),
                        int(et_query(et_root, enc + ["y"])),
                    )
                    enc2 = enc = ["encoding", "encodedSpace", "matrixSize"]
                    k_space_size = (
                        int(et_query(et_root, enc2 + ["x"])),
                        int(et_query(et_root, enc2 + ["y"])),
                    )
                    data_conv['img_size'] = crop_size
                    data_conv['kspace_size'] = k_space_size

            with open(os.path.join(self.args.engine.sensitivityMapsPath, file), 'rb') as f:
                data_ = h5py.File(f, 'r')
                if 'sens_maps' in data_:
                    data_conv['sens_maps'] = np.moveaxis(np.squeeze(np.stack(data_['sens_maps'][()])), -1, 1)
            self.data.append(data_conv)

    def index_files(self):
        extension = ".rawb" if self.args.task.useSimulatedData else ".h5"
        self.fileName = sorted(
            [f for f in os.listdir(self.path) if (os.path.isfile(os.path.join(self.path, f)) and (extension in f))])
        if self.args.task.preLoadData and not self.args.task.useSimulatedData:
            self.load_folder()
        elif not self.args.task.preLoadData and not self.args.task.useSimulatedData:
            self.load_file(0)
        elif self.args.task.useSimulatedData:
            self.get_anatomical_maps()

    def load_file(self, idx):
        file = self.fileName[idx]
        data_conv = {}
        with open(os.path.join(self.path, file), 'rb') as f:
            data_ = h5py.File(f, 'r')
            if 'kspace' in data_:
                data_conv['kspace'] = data_['kspace'][()]
            if 'reconstruction_rss' in data_:
                data_conv['reconstruction_rss'] = data_['reconstruction_rss'][()]
            if 'mask' in data_:
                data_conv['undersampling_mask'] = data_['mask'][()]
            if "ismrmrd_header" in data_:
                et_root = etree.fromstring(data_["ismrmrd_header"][()])
                enc = ["encoding", "reconSpace", "matrixSize"]
                crop_size = (
                    int(et_query(et_root, enc + ["x"])),
                    int(et_query(et_root, enc + ["y"])),
                )
                enc2 = ["encoding", "encodedSpace", "matrixSize"]
                k_space_size = (
                    int(et_query(et_root, enc2 + ["x"])),
                    int(et_query(et_root, enc2 + ["y"])),
                )
                data_conv['img_size'] = crop_size
                data_conv['kspace_size'] = k_space_size

        with open(os.path.join(self.args.engine.sensitivityMapsPath, file), 'rb') as f:
            data_ = h5py.File(f, 'r')
            if 'sens_maps' in data_:
                data_conv['sens_maps'] = np.moveaxis(np.squeeze(np.stack(data_['sens_maps'][()])), -1, 1)
        self.data = data_conv
        self.idx_control = idx

    def get_anatomical_maps(self):
        # fileList = sorted([f for f in os.listdir(self.path) if (os.path.isfile(os.path.join(self.path, f)) and (".rawb" in f))])
        # self.fileName = fileList
        # print(self.fileName)
        for file in self.fileName:
            raw_data = np.fromfile(os.path.join(self.path, file), dtype='int8', sep="")
            raw_data = np.reshape(raw_data, (362, 434, 362))
            self.data.append(raw_data)

    def get_existing_data(self, idx):

        if not self.args.task.preLoadData:
            data_ = self.data
        else:
            data_ = self.data[idx]
        rss_max = data_['reconstruction_rss'].max()
        k_space = torch.from_numpy(data_['kspace'])
        sens_map = torch.from_numpy(data_['sens_maps'])
        img_size = data_['img_size']
        k_space_size = data_['kspace_size']
        label_mod = Label_model_fastMRI(self.args, sens_map, img_size)
        coil_img, self.training_label, slices, self.mask, norm_factor = label_mod.generate_training_label(k_space, rss_max)
        self.training_signal = self.args.engine.signal_model.generate_training_signal(k_space, coil_img, slices,
                                                                                          self.training_label,
                                                                                          sens_map, img_size, k_space_size, norm_factor)



    def get_label(self, idx):
        if self.args.task.useSimulatedData:
            label_mod = Label_model_sim(self.args, self.data, self.sensitivityMap)
            if self.args.task.type == 'multicoil':
                [self.training_label, self.mask,
                 self.training_label_mulicoil, self.sens_map_slices] = label_mod.generate_simulated_training_label(idx)
            else:
                [self.training_label, self.mask] = label_mod.generate_simulated_training_label(idx)
        else:
            if not self.args.task.preLoadData:
                self.load_file(idx)
            self.get_existing_data(idx)
        return self.training_label, self.mask  # [self.mask, self.fileName[idx]]

    def get_signal(self, idx):
        if self.args.task.useSimulatedData:
            if self.args.task.type == 'multicoil':
                self.training_signal = self.args.engine.signal_model.generate_training_signal(self.training_label,
                                                                                              self.training_label_mulicoil, self.sens_map_slices)
            else:
                self.training_label_mulicoil = [0]
                self.sens_map_slices = []
                self.training_signal = self.args.engine.signal_model.generate_training_signal(self.training_label,
                                                                                              self.training_label_mulicoil, self.sens_map_slices)
        return self.training_signal

    def get_length(self):
        return len(self.fileName)
