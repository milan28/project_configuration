import h5py
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import torch
import torch.nn.functional as F
from math import prod
from EMCqMRI.core.utilities import image_utilities
from skimage.metrics import structural_similarity as ssim2
from project_configuration.utilities import evaluate_data
from project_configuration.utilities.plotter import plotter


useMask = True
mag_only = True
training = False
patch = 0

if training:
    # result = h5py.File('data/generated/estimated_data/v01_fully_sampledsubject04_crisp_v_epoch0.h5', 'r')
    result = h5py.File('data/generated/training/Sv045_epoch1000_simulatedsubject05_crisp_v_epoch_estimating_0_batch_9.h5', 'r')
    RIM = result['rimtraining']
    estimated_real_imag = np.moveaxis(RIM['estimated'][patch], -1, 0)
else:
    result = h5py.File('data/generated/simulated_data/Sv062_epoch1000_simulatedsubject43_crisp_v_epoch_estimating_2_batch_9.h5',
                       'r')
    RIM = result['rimtesting']
    estimated_real_imag = np.moveaxis(RIM['estimated'], -1, 0)

estimated_abs = np.expand_dims((np.sqrt(estimated_real_imag[0] ** 2 + estimated_real_imag[1] ** 2)), 0)
estimated = np.concatenate((estimated_real_imag, estimated_abs), 0)
label_real_imag = np.moveaxis(RIM['labels'][patch], -1, 0)
label_abs = np.expand_dims((np.sqrt(label_real_imag[0] ** 2 + label_real_imag[1] ** 2)), 0)
label = np.concatenate((label_real_imag, label_abs), 0)
initial_guess_real_imag = np.moveaxis(RIM['initial'][patch], -1, 0)
initial_guess_abs = np.expand_dims((np.sqrt(initial_guess_real_imag[0] ** 2 + initial_guess_real_imag[1] ** 2)), 0)
initial_guess = np.concatenate((initial_guess_real_imag, initial_guess_abs), 0)
if useMask:
    mask = RIM['mask'][patch][:]
    if mask.shape[0] == 3:
        mask = (mask[0] == 1) | (mask[1] == 1) | (mask[2] == 1)
else:
    mask = True
keys = ['Real', 'Imaginary', 'Magnitude', "Aliased image"]
cmap = plt.get_cmap('PuOr')
diff = estimated - label

cmap = plt.get_cmap('PuOr')
# norm = mpl.colors.Normalize(vmin=diff.min(), vmax=diff.max())

rows_table = ['NMSE', 'PSNR', 'SSIM']
for i in range(3):
    if mag_only:
        i = 2
    table = np.ones((3, 2))
    NMSE = evaluate_data.nmse(label[i], estimated[i], mask)
    # SSIM = ssim(label[i], estimated[i])
    PSNR = evaluate_data.psnr(label[i], estimated[i], mask)
    SSIM = evaluate_data.ssim(label[i], estimated[i], mask)
    table[0, 0] = NMSE
    table[1, 0] = PSNR
    table[2, 0] = SSIM
    print("NMSE of " + keys[i] + " " + str(NMSE))
    print("PSNR of " + keys[i] + " " + str(PSNR))
    print("SSIM of " + keys[i] + " " + str(SSIM))
    if i == 2:
        SSIM_init = evaluate_data.ssim(label[i], initial_guess[i], mask)
        NMSE_init = evaluate_data.nmse(label[i], initial_guess[i], mask)
        PSNR_init = evaluate_data.psnr(label[i], initial_guess[i], mask)
        table[0, 1] = NMSE_init
        table[1, 1] = PSNR_init
        table[2, 1] = SSIM_init

    est_flat = estimated[i].flatten()
    label_flat = label[i].flatten()
    vmax = max([estimated[i].max(), label[i].max()])
    vmin = min([estimated[i].min(), label[i].min()])
    v_diff = abs(diff[i]).max()
    fig1, axs = plt.subplots(2, 3, figsize=(15, 10))
    # fig1.suptitle(keys[i])
    fig1.suptitle('Training data')
    im1 = axs[0, 0].imshow(label[i], cmap='gray', vmax=vmax, vmin=vmin)
    fig1.colorbar(im1, ax=axs[0, 0])
    axs[0, 0].set_title("Ground Truth")
    im2 = axs[0, 1].imshow(estimated[i], cmap='gray', vmax=vmax, vmin=vmin)
    fig1.colorbar(im2, ax=axs[0, 1])
    axs[0, 1].set_title("Inferred image")
    im3 = axs[1, 0].imshow(diff[i], cmap=cmap, vmax=v_diff, vmin=-v_diff)
    fig1.colorbar(im3, ax=axs[1, 0])
    axs[1, 0].set_title("Difference")
    axs[1, 1].scatter(est_flat, label_flat, marker=".", alpha=0.1)
    axs[1, 1].plot([-3, 3], [-3, 3], color='black')
    axs[1, 1].set_xlabel('Estimated')
    axs[1, 1].set_xlim(min(est_flat), max(est_flat))
    axs[1, 1].set_ylim(min(est_flat), max(est_flat))
    axs[1, 1].set_ylabel("Inferred")
    axs[1, 1].set_title("Scatter plot")
    im4 = axs[0, 2].imshow(initial_guess[i], cmap='gray', vmax=vmax, vmin=vmin)
    fig1.colorbar(im4, ax=axs[0, 2])
    axs[0, 2].set_title("Aliased Image")
    table = np.around(table, 5)
    the_table = axs[1,2].table(cellText=table,
                      rowLabels=rows_table,
                      colLabels=[keys[i]] + ['Aliased image'],
                      loc='center')
    # fig1.delaxes(axs[1][2])
    the_table.auto_set_font_size(False)
    axs[1,2].axis('off')
    the_table.auto_set_font_size(False)
    the_table.set_fontsize(15)
    # plt.box(on=None)
    the_table.scale(0.8, 2.5)
    plt.tight_layout()
    plt.show()
    if i == 2:
        break
