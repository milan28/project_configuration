import EMCqMRI.core.utilities.dataset_utilities as datasetUtilities
import numpy as np
import torch
import torch.fft
import random
import math
import logging
from project_configuration.utilities import fourier_transforms as ft
from project_configuration.utilities.transforms import center_crop
import skimage
from scipy import ndimage
from skimage.segmentation import flood, flood_fill
from random import randrange
import matplotlib.pyplot as plt
from project_configuration.utilities.plotter import plotter


class Label_model():
    def __init__(self, args, sens_map, img_size):
        super(Label_model, self).__init__()
        self.args = args
        self.sens_map = sens_map
        self.img_size = img_size

    def generate_training_label(self, k_space, rss_max):
        coils_img_1 = self.transform_k_space_to_image(k_space)
        self.args.task.pngr = datasetUtilities.get_rand_seed(self.args.task.useRandomSeed)
        if self.args.task.usePatches:
            coils_img, slices = self.patch_extractor(coils_img_1)
        else:
            coils_img, self.slices = self.slice_extractor(coils_img_1)
            norm = 0.6/rss_max
            norm_factor = self.args.task.pngr.normal(norm, norm/10)
        coil_img_norm = coils_img * norm_factor
        training_label, mask = self.create_ground_truth(coil_img_norm)
        return coil_img_norm, training_label.float().to(self.args.engine.device), self.slices, mask, norm_factor

    def transform_k_space_to_image(self, k_space):
        slice_img_list = []
        for slice in range(k_space.shape[0]):
            coils_img_list = []
            for coil in range(k_space.shape[1]):
                coil_k_space = torch.view_as_real(k_space[slice, coil])
                coils_img_list.append(ft.ifft2c_new(coil_k_space))
            coils_img = torch.stack(coils_img_list)
            slice_img_list.append(coils_img)
        img = torch.stack(slice_img_list)
        return img

    def create_ground_truth(self, img_coil):
        gt_list = []
        mask_list = []
        img_coil_complex = center_crop(torch.view_as_complex(img_coil), self.img_size)
        sens_maps_all = center_crop(self.sens_map, self.img_size)

        for o, img_coil_1 in enumerate(img_coil_complex):
            sens_maps = sens_maps_all[self.slices[o]]
            coil_combined = torch.zeros(self.img_size, dtype=torch.cfloat)
            for sens_, img_ in zip(sens_maps, img_coil_1):
                coil_combined += img_ * torch.conj(sens_)
            gt_list.append(coil_combined)
            max_ind_ = torch.argmax(coil_combined.abs())
            max_ind = self.unravel_index(max_ind_, coil_combined.shape)
            max_val = coil_combined.abs().max()
            mask_list.append(torch.from_numpy(
                ndimage.binary_fill_holes(flood(coil_combined.abs(), max_ind, tolerance=max_val - 0.025),
                                                  structure=np.ones((4, 4)))))
        return torch.view_as_real(torch.stack(gt_list)), torch.stack(mask_list)

    def rss(self, img_coil):
        rss_list = []
        rss_max = []
        img_coil_complex = torch.view_as_complex(img_coil)
        for img_coil_1 in img_coil_complex:
            rss = center_crop(torch.sqrt((abs(img_coil_1) ** 2).sum(0)), self.img_size)
            rss_list.append(rss)
            rss_max.append(rss.max())
        return torch.stack(rss_list), torch.stack(rss_max)

    def patch_extractor(self, img):
        patches_list = []
        slice_list = []
        for r in range(self.args.task.numberOfPatches):
            slice = self.args.task.pngr.randint(img.shape[0])
            slice_list.append(slice)
            border = (img.shape[3] - img.shape[4]) / 2
            y_middle = self.args.task.pngr.randint(int(border + self.args.task.patchSize / 2),
                                                   int(img.shape[3] - border - self.args.task.patchSize / 2))
            x_middle = self.args.task.pngr.randint(int(self.args.task.patchSize / 2),
                                                   int(img.shape[4] - self.args.task.patchSize / 2))
            y_location = np.arange(int(y_middle - self.args.task.patchSize / 2),
                                   int(y_middle + self.args.task.patchSize / 2),
                                   dtype=int)
            x_location = np.arange(int(x_middle - self.args.task.patchSize / 2),
                                   int(x_middle + self.args.task.patchSize / 2),
                                   dtype=int)
            patch_1 = img[slice, :, :, y_location]
            patch_2 = patch_1[:, :, :, x_location]
            patches_list.append(patch_2)
        patches = torch.stack(patches_list)
        slices = torch.tensor(slice_list)
        return patches, slices

    def slice_extractor(self, img):
        img_list = []
        if img.shape[0] - 6 > self.args.task.numberOfPatches:
            slices = self.args.task.pngr.choice(range(0, img.shape[0] - 3), self.args.task.numberOfPatches,
                                                replace=False)
        else:
            slices = self.args.task.pngr.randint(0, img.shape[0] - 3, self.args.task.numberOfPatches)
        for slice in slices:
            img_list.append(img[slice])
        img_slice = torch.stack(img_list)
        logging.error(slices)
        return img_slice, slices

    def unravel_index(self, index, shape):
        out = []
        for dim in reversed(shape):
            out.append(index % dim)
            index = index // dim
        return tuple(reversed(out))
