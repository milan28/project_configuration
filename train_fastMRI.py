from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import logging
from EMCqMRI.core.configuration.core import pll_configuration
from EMCqMRI.core.engine import build_model as core_build
from EMCqMRI.core.engine import train_model
from EMCqMRI.core.utilities import image_utilities
from project_configuration.utilities import NMSE
import project_configuration.utilities.SSIM as SSIM
import torch

def override_model(configObject):
    ################################################################
    # SET MODELS. OVERRIDES CONFIGURATION FROM FILE
    from project_configuration.dataset import dataset_sim
    configObject.args.engine.dataset = dataset_sim.MyDataset(configObject)

def override_signal(configObject):
    from project_configuration.models import signal_model_fastMRI
    configObject.args.engine.signal_model = signal_model_fastMRI.Signal_model(configObject)

def override_rim(configObject):
    from project_configuration.inference_model.rim import rim_modified
    configObject.args.engine.inference_model = rim_modified.Rim(configObject)

def override_likelihood(configObject):
    from project_configuration.models.likelihood_model.gaussian import gaussian
    configObject.args.engine.likelihood_model = gaussian.Gaussian(configObject)


if __name__ == '__main__':
    configurationObj = pll_configuration.Configuration('TRAINING')
    configurationObj.args.engine.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    configurationObj.sensitivityMap = 0
    override_model(configurationObj)
    override_signal(configurationObj)
    override_rim(configurationObj)
    override_likelihood(configurationObj)
    configObject = core_build.make(configurationObj)
    if configurationObj.args.engine.lossFunction == 'NMSE':
        configObject.args.engine.objective_fun = NMSE.NMSE
    elif configurationObj.args.engine.lossFunction == 'SSIM':
        configObject.args.engine.objective_fun = SSIM.SSIM
    logging.info('{} model succesfully built.'.format(configurationObj.args.engine.inference_model.__name__))
    logging.info('Starting training....')
    tr_model = train_model.Train(configObject)
    tr_model.execute()
