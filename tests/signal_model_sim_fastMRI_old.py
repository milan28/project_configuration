import torch
import torch.fft
import numpy as np
import datetime
import matplotlib.pyplot as plt
from project_configuration.utilities.subsample import create_mask_for_mask_type
import project_configuration.utilities.fourier_transforms as fft


class Signal_model():
    def __init__(self, configObject):
        self.args = configObject.args

    def generate_training_signal(self, training_label):
        """ This method generates the undersampled k-space images"""
        undersampled_k_space_patch = []
        undersampling_mask = []
        img_size = training_label[0, 0, ...].shape
        self.args.task.sensitivityMask = self.sensitivtyMask(img_size).to(self.args.engine.device)
        for label_patch_0 in training_label:
            mask = self.create_undersampling_mask(img_size, self.args.task.kLinesMiddle, self.args.task.kDensity)
            undersampling_mask.append(mask)
            label_patch_sens = label_patch_0 * self.args.task.sensitivityMask
            label_patch_1 = torch.movedim(label_patch_sens, 0, -1)
            k_space_image = fft.fft2c_new(label_patch_1.contiguous())
            k_space_image = torch.view_as_complex(k_space_image)
            self.generate_complex_noise(img_size)
            k_image_images_noisy = torch.add(k_space_image, self.args.task.noiseMap)
            k_undersampled = torch.multiply(k_image_images_noisy, mask)
            k_undersampled_re_im_split = torch.stack((k_undersampled.real, k_undersampled.imag))
            undersampled_k_space_patch.append(k_undersampled_re_im_split)
        training_signal = torch.stack(undersampled_k_space_patch).float().to(self.args.engine.device)
        self.args.task.undMask = undersampling_mask
        return training_signal

    def generate_complex_noise(self, img_size):
        k_space_size_x = img_size[0]
        k_space_size_y = img_size[1]
        empty_noise_map = torch.zeros(k_space_size_x, k_space_size_y, 2).to(self.args.engine.device)
        noise_map = torch.view_as_complex(empty_noise_map.data.normal_(0, std=self.args.task.sigmaNoise))
        self.args.task.noiseMap = noise_map

    def create_undersampling_mask(self, img_size, lines_middle, k_density):
        k_space_size_x = img_size[0]
        k_space_size_y = img_size[1]
        shape = [img_size[0], img_size[1], 1]
        if self.args.task.maskType == 'original':
            k_lines_middle = lines_middle
            middle = k_space_size_x // 2
            k_density_inv = k_density ** (-1)
            # k_density_inv = torch.normal(k_density, 0.1, size=(1, 1)) ** (-1)
            mask = torch.zeros(k_space_size_x, k_space_size_y)
            sampled_lines = np.round(np.arange(0, k_space_size_x - 1, k_density_inv))
            mask[sampled_lines, :] = 1
            mask[int(middle - (k_lines_middle - 1) / 2): int(middle + (k_lines_middle) / 2), :] = 1
        if self.args.task.maskType == 'equispaced':
            acceleration = [1 / self.args.task.kDensity]
            mask_func = create_mask_for_mask_type(self.args.task.maskType, [self.args.task.kCenter_fractions],
                                                  acceleration)
            mask = mask_func(shape)
        if self.args.task.maskType == 'random':
            acceleration = [1 / self.args.task.kDensity]
            mask_func = create_mask_for_mask_type(self.args.task.maskType, [self.args.task.kCenter_fractions],
                                                  acceleration)
            if self.args.task.useRandomSeedMask:
                tim = datetime.datetime.now()
                randseed = tim.hour * 10000 + tim.minute * 100 + tim.second + tim.microsecond
                mask = mask_func(shape, randseed)
            else:
                mask = mask_func(shape, 10)
        return mask.to(self.args.engine.device)


    def generateUndKspace(self, predicted_weighted_image_0, tau, index):  # signal
        predicted_weighted_image_sens = torch.multiply(predicted_weighted_image_0,self.args.task.sensitivityMask)
        predicted_weighted_image_1 = torch.movedim(predicted_weighted_image_sens, 0, -1)
        k_space = torch.view_as_complex(fft.fft2c_new(predicted_weighted_image_1.contiguous()))
        k_space_und_undersampled_ = torch.multiply(k_space, self.args.task.undMask[index])
        k_und_real_imag_split = torch.stack((k_space_und_undersampled_.real, k_space_und_undersampled_.imag))
        return k_und_real_imag_split.float()

    def initializeParameters(self, signal):  # label
        img_for_each_patch = []
        for label_patch_0 in signal:
            label_patch_1 = torch.movedim(label_patch_0, 0, -1)
            img_real_imag_together = torch.view_as_complex(fft.ifft2c_new(label_patch_1.contiguous()))
            img_real_imag_together_sens_corrected = torch.div(img_real_imag_together, self.args.task.sensitivityMask)
            w_image_complex_re_im_split = torch.stack(
                (img_real_imag_together_sens_corrected.real, img_real_imag_together_sens_corrected.imag))
            img_for_each_patch.append(w_image_complex_re_im_split)
        label_guess = torch.stack(img_for_each_patch)
        return label_guess.float()

    def sensitivtyMask(self, img_size):
        mask = np.zeros(img_size)
        radius = self.args.task.sensitivityMaskRadius
        middle_point = [self.args.task.sensitivityMaskMiddleX, self.args.task.sensitivityMaskMiddleY]
        for w in range(img_size[0]):
            for h in range(img_size[1]):
                distance = np.sqrt((h - middle_point[0]) ** 2 + (w - middle_point[1]) ** 2)
                if distance < radius:
                    mask[w, h] = -1 / radius * distance + 1
        return torch.from_numpy(mask)

