import matplotlib.pyplot as plt
import numpy as np
import h5py
import matplotlib as mpl
experiment = '4x acceleration'
f = h5py.File('data/evaluation/trained_Sv033_infer_fastMRI_multicoil_val.h5', 'r')
epochs = f.keys()
mpl.rc('font', family='Futura Lt BT')
data = f[list(epochs)[0]]
metrics = data.keys()
ticks = range(1, len(epochs)+2)
epoch_name = []
for epoch in epochs:
    if epoch[0:2] == '00':
        epoch_name.append(epoch[2:4])
    elif epoch[0] == '0':
        epoch_name.append(epoch[1:4])
    else:
        epoch_name.append(epoch)
log = [1,0,0]
for x in range(3):
    eval_list = []
    metric = list(metrics)[x*2]
    init = f[list(epochs)[0]][metric + '_init'][:]
    eval_list.append(init.flatten())
    for epoch in epochs:
        eval = f[epoch][metric][:]
        eval_list.append(eval.flatten())

    fig, ax = plt.subplots()
    ax.boxplot(eval_list)
    plt.xticks(ticks, ['zero filled'] + epoch_name)
    ax.set_title(experiment)
    if log[x]:
        ax.set_yscale('log')
    # ax.ticklabel_format(axis='y', style='plain')
    ax.set_xlabel('epochs')
    ax.set_ylabel(metric)
    plt.tight_layout()
    plt.show()

