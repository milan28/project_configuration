import torch
import torch.fft
import numpy as np
import datetime
import matplotlib.pyplot as plt
from project_configuration.utilities.subsample import create_mask_for_mask_type
import project_configuration.utilities.fourier_transforms as fft
import math
import EMCqMRI.core.utilities.dataset_utilities as datasetUtilities
from project_configuration.utilities.transforms import center_crop
from project_configuration.utilities.plotter import plotter

class Signal_model():
    def __init__(self, configObject):
        self.args = configObject.args
        self.sensitivityMap = configObject.sensitivityMap
        self.sensitivityMap_CUDA = configObject.sensitivityMap_CUDA

    def generate_training_signal(self, training_label, training_label_multicoil, sens_map_slices):
        """ This method generates the undersampled k-space images"""
        self.sens_map_slices = sens_map_slices
        self.args.task.pngr = datasetUtilities.get_rand_seed(self.args.task.useRandomSeed)
        undersampled_k_space_patch = []
        img_size = training_label_multicoil.shape[2:4]
        self.undMask = self.create_undersampling_mask(img_size)
        self.undMask_CUDA = self.undMask.to(self.args.engine.device)
        if self.args.task.type == 'multicoil':
            for label_patch_ in training_label_multicoil:
                k_space_coil_list = []
                for coil_img in label_patch_:
                    k_space_coil = fft.fft2c_new(coil_img)
                    sigma_noise = 1 / self.args.task.SNR
                    noise = self.generate_complex_noise(img_size, sigma_noise)
                    k_image_noisy = torch.add(torch.view_as_complex(k_space_coil), noise)
                    k_space_coil_list.append(k_image_noisy)
                k_images_noisy = torch.stack(k_space_coil_list)
                k_undersampled = torch.multiply(k_images_noisy, self.undMask)
                undersampled_k_space_patch.append(k_undersampled)
            training_signal = torch.stack(undersampled_k_space_patch)
        else:
            for label_patch_ in training_label:
                k_space_image = fft.fft2c_new(label_patch_)
                std_k_space = torch.sqrt((abs(torch.view_as_complex(k_space_image)) ** 2).mean())
                sigma_noise = std_k_space/self.args.task.SNR
                # sigma_noise = (abs(torch.view_as_complex(k_space_image)).numpy()) / self.args.task.SNR
                noise = self.generate_complex_noise(img_size, sigma_noise)
                k_image_images_noisy = torch.add(torch.view_as_complex(k_space_image), noise)
                k_undersampled = torch.multiply(k_image_images_noisy, self.undMask)
                undersampled_k_space_patch.append(k_undersampled)
            training_signal = torch.stack(undersampled_k_space_patch)
        return torch.view_as_real(training_signal).type(torch.float32).to(self.args.engine.device)

    def generate_complex_noise(self, img_size, sigma):
        k_space_size_x = img_size[0]
        k_space_size_y = img_size[1]
        noise_map = torch.view_as_complex(torch.from_numpy(self.args.task.pngr.normal(0, sigma, (k_space_size_x,  k_space_size_y, 2))))
        return noise_map

    def create_undersampling_mask(self, img_size):
        shape = [img_size[0], img_size[1], 1]

        if isinstance(self.args.task.kDensity, list):
            acceleration = [1 / self.args.task.pngr.choice(np.array(self.args.task.kDensity))]
        else:
            acceleration = [1 / self.args.task.kDensity]
        if acceleration[0] == 4:
            middle_lines = 0.08
        elif acceleration[0] == 8:
            middle_lines = 0.04
        else:
            middle_lines = self.args.task.kCenter_fractions
        if self.args.task.maskType == 'equispaced':
            mask_func = create_mask_for_mask_type(self.args.task.maskType, [middle_lines],
                                                  acceleration,self.args.task.pngr)
            mask = mask_func(shape)
        elif self.args.task.maskType == 'random':
            mask_func = create_mask_for_mask_type(self.args.task.maskType, [middle_lines],
                                                  acceleration)
            if self.args.task.useRandomSeedMask:
                tim = datetime.datetime.now()
                randseed = tim.hour * 10000 + tim.minute * 100 + tim.second + tim.microsecond
                mask = mask_func(shape, randseed)
            else:
                mask = mask_func(shape, 10)
        return mask

    def generateUndKspace(self, predicted_weighted_image, index):  # signal
        if self.args.task.type == 'multicoil':
            coil_img_list = []
            k_space_coil_list = []
            predicted_weighted_image_complex = torch.view_as_complex(predicted_weighted_image)
            for coil_sens in self.sensitivityMap_CUDA[self.sens_map_slices[index]]:
                coil_img = predicted_weighted_image_complex * coil_sens
                coil_img_list.append(torch.view_as_real(coil_img))
                k_space_coil = fft.fft2c_new(torch.view_as_real(coil_img))
                k_space_coil_list.append(k_space_coil)
            k_images = torch.stack(k_space_coil_list)
            predicted_k_space = torch.multiply(torch.view_as_complex(k_images), self.undMask_CUDA)
        else:
            k_space = fft.fft2c_new(predicted_weighted_image)
            predicted_k_space = torch.multiply(torch.view_as_complex(k_space), self.undMask_CUDA)

        return torch.view_as_real(predicted_k_space).type(torch.float32)

    def initializeParameters(self, signal, indexes):  # label
        img_for_each_patch = []
        all_coil_img_list = []
        index_0 = indexes[-1]
        for index, signal_patch in enumerate(signal):
            if self.args.task.type == 'multicoil':
                coil_img_list = []
                for coil in signal_patch:
                    coil_img = fft.ifft2c_new(coil)
                    coil_img_list.append(torch.view_as_complex(coil_img))
                all_coil_img_list.append(torch.stack(coil_img_list))
                guess = self.create_ground_truth(torch.stack(coil_img_list), (index+index_0))
                img_for_each_patch.append(guess)
            else:
                img_real = fft.ifft2c_new(signal_patch)
                img_complex = torch.view_as_complex(img_real)
                img_for_each_patch.append(img_complex / (abs(img_complex).max()))

        return torch.view_as_real(torch.stack(img_for_each_patch)).type(torch.float32), torch.view_as_real(torch.stack(all_coil_img_list)).cpu()

    def create_ground_truth(self, img_coil, index):
        img_size = tuple(img_coil.shape[1:3])
        sens_maps = self.sensitivityMap_CUDA[self.sens_map_slices[index]]
        coil_combined = torch.zeros(img_size, dtype=torch.cfloat).to(self.args.engine.device)
        for sens_, img_ in zip(sens_maps, img_coil):
            coil_combined += img_ * torch.conj(sens_)
        return coil_combined
