import h5py
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import torch
import torch.nn.functional as F
from math import prod
from EMCqMRI.core.utilities import image_utilities
from skimage.metrics import structural_similarity as ssim2
import torch


def nmse(label, estimated):
    return np.sum(((estimated - label) ** 2)) / np.sum(label ** 2)


def ssim(estimated, label):
    estimated = torch.unsqueeze(estimated, (0, 1))
    label = torch.unsqueeze(label, (0, 1))
    data_range = abs(label).max()
    k1 = 0.01
    k2 = 0.03
    win_size = 7
    NP = win_size ** 2
    cov_norm = NP / (NP - 1)
    w = torch.ones(win_size, win_size) / win_size ** 2

    C1 = (k1 * data_range) ** 2
    C2 = (k2 * data_range) ** 2
    ux = F.conv2d(estimated, w)  # typing: ignore
    uy = F.conv2d(label, w)  #
    uxx = F.conv2d(estimated * estimated, w)
    uyy = F.conv2d(label * label, w)
    uxy = F.conv2d(estimated * label, w)
    vx = cov_norm * (uxx - ux * ux)
    vy = cov_norm * (uyy - uy * uy)
    vxy = cov_norm * (uxy - ux * uy)
    A1, A2, B1, B2 = (
        2 * ux * uy + C1,
        2 * vxy + C2,
        ux ** 2 + uy ** 2 + C1,
        vx + vy + C2,
    )
    D = B1 * B2
    S = (A1 * A2) / D

    return 1 - S.mean()


def psnr(estimated, label):
    estimated = estimated.numpy()
    label = label.numpy()
    MSE = ((estimated - label) ** 2).mean(axis=None)
    return 10 * np.log10((abs(label).max() ** 2) / MSE)


result = h5py.File('data/generated/training/Fv001file_brain_AXT1_201_6002783_epoch25.h5')
RIM = result['rimtraining']

# result = h5py.File('data/generated/estimated_data/v107subject54_crisp_v_epoch1600.h5', 'r')
# # RIM = result['rimtesting']
patch = 0

# result = h5py.File('sens_maps.h5')


estimated = torch.view_as_complex(torch.from_numpy(RIM['estimated'][patch]))
label = torch.view_as_complex(torch.from_numpy(RIM['labels'][patch]))
initial_guess = torch.view_as_complex(torch.from_numpy(RIM['initial'][patch]))
result.close()
keys = ['Ground Truth', 'Reconstruction', 'Initial guess']
cmap = plt.get_cmap('PuOr')
diff = estimated - label
all = [label, estimated, initial_guess]
table = np.ones((3, 2))
rows_table = ['NMSE', 'PSNR', 'SSIM']

for i in range(2):
    NMSE = nmse(all[i+1].abs().numpy(), label.abs().numpy())
    PSNR = psnr(all[i+1].abs(), label.abs())
    SSIM = ssim2(all[i+1].abs().numpy(), label.abs().numpy(), data_range=label.abs().max().numpy(), win_size=7)
    table[0, i] = NMSE
    table[1, i] = PSNR
    table[2, i] = SSIM
est_flat = estimated.abs().flatten()
label_flat = label.abs().flatten()
vmax = initial_guess.abs().max()
vmin = initial_guess.abs().min()
v_diff = abs(diff).max()
fig1, axs =  plt.subplots(figsize=(15.0, 15.0) , nrows=4, ncols=2, sharey=True)
# for row, big_ax in enumerate(axs, start=1):
#     big_ax.set_title(keys[row], fontsize=16)
#     # Turn off axis lines and ticks of the big subplot
#     # obs alpha is 0 in RGBA string!
#     big_ax.tick_params(labelcolor=(1.,1.,1., 0.0), top='off', bottom='off', left='off', right='off')
#     # removes the white frame
#     big_ax._frameon = False
for o, img in enumerate(all):
    im1 = axs[o, 0].imshow(img.abs().numpy(), cmap='gray', vmax=vmax, vmin=vmin)
    fig1.colorbar(im1, ax=axs[o, 0])
    axs[o, 0].set_title("Magnitude")
    im2 = axs[o, 1].imshow(img.angle().numpy(), vmax=vmax, vmin=vmin)
    fig1.colorbar(im2, ax=axs[o, 1])
    axs[o, 1].set_title("Phase")
im3 = axs[3, 0].imshow(diff.abs().numpy(), cmap=cmap, vmax=v_diff, vmin=-v_diff)
fig1.colorbar(im3, ax=axs[3, 0])
axs[3, 0].set_title("Difference")
axs[3, 1].scatter(est_flat.numpy(), label_flat.numpy(), marker=".", alpha=0.1)
axs[3, 1].plot([-3, 3], [-3, 3], color='black')
axs[3, 1].set_xlabel('Estimated')
axs[3, 1].set_xlim(min(est_flat), max(est_flat))
axs[3, 1].set_ylim(min(est_flat), max(est_flat))
axs[3, 1].set_ylabel("Inferred")
axs[3, 1].set_title("Scatter plot")

plt.show()
table = np.around(table, 5)
the_table = plt.table(cellText=table,
                      rowLabels=rows_table,
                      colLabels=keys,
                      loc='center')
ax = plt.gca()
ax.get_xaxis().set_visible(False)
ax.get_yaxis().set_visible(False)
the_table.auto_set_font_size(False)
the_table.set_fontsize(12)
plt.box(on=None)
the_table.scale(0.8, 2)
plt.show()
