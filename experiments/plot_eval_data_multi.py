import matplotlib.pyplot as plt
import numpy as np
import h5py
import os
import matplotlib as mpl

def set_box_color(bp, color):
    plt.setp(bp['boxes'], color=color)
    plt.setp(bp['whiskers'], color=color)
    plt.setp(bp['caps'], color=color)
    plt.setp(bp['medians'], color=color)

def load_folder(fileName, path):
    mpl.rc('font', family='CMU Sans Serif')
    mpl.rc('xtick', labelsize=30)
    mpl.rc('axes', labelsize=15)
    list_metrics = {'NMSE' : {},'PSNR' : {}, 'SSIM' : {}}
    metrics_2 = ['NMSE', 'PSNR', 'SSIM']
    label2 = ['lr: 4e-3', 'lr: 2e-3', 'lr: 1e-3', 'lr: 5e-4']
    for file in fileName:
        experiment = 'baseline fastMRI vs baseline sim vs final model'
        data_ = h5py.File(os.path.join(path, file), 'r')
        epochs = data_.keys()
        data = data_[list(epochs)[0]]
        metrics = data.keys()
        ticks = range(1, len(epochs) + 2)
        epoch_name = []
        for epoch in epochs:
            if epoch[0:2] == '00':
                epoch_name.append(epoch[2:4])
            elif epoch[0] == '0':
                epoch_name.append(epoch[1:4])
            else:
                epoch_name.append(epoch)
        all_data = []
        for x, metric in enumerate(metrics_2):
            list_metrics[metric][file] = []
            init = data_[list(epochs)[0]][metric + '_init'][:]
            list_metrics[metric][file] = [init.flatten().tolist()]
            for o, epoch in enumerate(epochs):
                eval = data_[epoch][metric][:]
                list_metrics[metric][file].append(eval.flatten().tolist())
    fig1, axs = plt.subplots(3, 1, figsize=(12, 10))
    index1 = [0, 1, 2]
    index2 = [0, 0, 0]
    log = [1, 0, 0]
    for met, i1, i2, l1 in zip(list_metrics, index1, index2, log):
        plt.figure()
        met_ = list_metrics[met]
        nr = len(met_)
        wide = 0
        colors = ['#f03b20', '#3182bd', '#31a354', '#bf21a5', '#ffa500']
        for epochs, color, lab in zip(met_, colors, label2):
            bpr = axs[i1,i2].boxplot(met_[epochs], positions=np.array(range(len(met_[epochs]))) * nr + wide, sym='', widths=0.6)

            set_box_color(bpr, color)
            wide += 0.8
            axs[i1,i2].plot([], c=color, label=lab)
            axs[i1,i2].legend()
        axs[i1, i2].set_title(met)
        if l1:
            axs[i1,i2].set_yscale('log')
        axs[i1,i2].set_xlabel('epochs')
        # axs[i1,i2].set_ylabel(met)

        axs[i1,i2].set_xticks(range(0, (len(met_[epochs]))*nr, nr))
        axs[i1, i2].set_xticklabels(['initial'] + epoch_name)
    fig1.delaxes(axs[1][1])
    plt.title('Learning rate optimization')
    plt.tight_layout()
    plt.show()

    data_.close()

    return data


def index_files(path):
    extension = ".h5"
    fileName = sorted(
        [f for f in os.listdir(path) if (os.path.isfile(os.path.join(path, f)) and (extension in f))])
    data = load_folder(fileName, path)
    return data


path = 'C:/Users/milan/OneDrive/Documenten/Thesis/EMC_qMRI_reconstruction/project_configuration/data/evaluation/simulated/'

list_with_excel_data = index_files(path)



