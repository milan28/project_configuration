from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from .rnn import Rnn
from EMCqMRI.core.base import base_inference_model
from EMCqMRI.core.utilities.core_utilities import ProgressBarWrap
import torch
from torch.autograd import Variable
import torch.nn as nn
import matplotlib.pyplot as plt


class Rim(base_inference_model.InferenceModel, nn.Module):
    """
        Class Implementing the RIM model.
        Methods:
            - setOpts
                inputs: a Dict containing the key and value for a new configuration setting
            - forward
                inputs: signal (measured signal); args (options containing, at least args.batchSize and args.device)
                outputs: Estimated parameters
    """

    def __init__(self, configObject):
        super(Rim, self).__init__()
        self.__name__ = 'RIM'
        self.__require_initial_guess__ = True
        self.args = configObject.args
        self.__buildNetwork__()

    def __buildNetwork__(self):
        """
            Hidden method that instanciates a version of the RNN module based on the configuration file
        """
        self.rnn = Rnn(self.args.inference.inputChannels,
                       self.args.inference.outputChannelsLayer1,
                       self.args.inference.outputChannelsLayer2,
                       self.args.inference.outputChannelsLayer3,
                       self.args.inference.outputChannels,
                       self.args).to(self.args.engine.device)

    def __initHidden__(self, signal):
        """
            Initialises all hidden states in the network
        """
        shape_input = torch.tensor((signal.shape)[1:3])
        shape_hs1 = [1, self.args.engine.batchSizeIter*int(torch.prod(shape_input)), self.args.inference.outputChannelsLayer1]
        shape_hs2 = [1, self.args.engine.batchSizeIter*int(torch.prod(shape_input)), self.args.inference.outputChannelsLayer3]
        st_1 = Variable(torch.zeros(tuple(shape_hs1)).to(device=self.args.engine.device))
        st_2 = Variable(torch.zeros(tuple(shape_hs2)).to(device=self.args.engine.device))




        return [st_1, st_2]

    def __getGradients__(self, signal, maps, indexes, inference_step):
        """
            Compute and return gradients of the Likelihood Function w.r.t. parameter maps
        """
        gradientParamBatch = []
        for batch in range(len(maps)):
            index = indexes[1] + batch
            clonedMaps = Variable(maps[batch].clone(), requires_grad=True)
            gradientParamBatch.append(self.args.engine.likelihood_model.gradients(self.args, signal[batch], clonedMaps, index,  inference_step, batch))
        paramGrad = torch.stack(gradientParamBatch)
        paramGrad[torch.isnan(paramGrad)] = 0
        paramGrad[torch.isinf(paramGrad)] = 0
        return paramGrad

    @ProgressBarWrap
    def update_bar(self, loss, args):
        return -1

    def forward(self, inputs, indexes):
        signal = inputs[0]
        kappa = inputs[1]

        hidden = self.__initHidden__(kappa)
        estimates = []
        for _ in range(self.args.inference.inferenceSteps):
            paramGrad = self.__getGradients__(signal, kappa, indexes, _)
            input = torch.movedim(torch.cat([kappa, paramGrad], -1), -1, -3).contiguous()
            dx, hidden = self.rnn.forward(input, hidden)
            # kappa = kappa
            kappa = kappa + dx

            if self.args.engine.stateName == "training":
                estimates.append(kappa)
            if self.args.engine.stateName == "testing":
                self.update_bar('-', self.args)

        if self.args.engine.stateName == "validation" or self.args.engine.stateName == "testing":
            estimates = kappa[0]

        return estimates
