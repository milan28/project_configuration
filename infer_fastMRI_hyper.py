from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import logging
from EMCqMRI.core.configuration.core import pll_configuration
from EMCqMRI.core.engine import build_model as core_build
from EMCqMRI.core.engine import estimate
from project_configuration.utilities import NMSE
from project_configuration.utilities import evaluate_data
from skimage.metrics import structural_similarity as ssim2
from project_configuration.utilities import converter
import numpy as np
import torch
import matplotlib.pyplot as plt
import h5py
import project_configuration.utilities.SSIM as SSIM


def override_model(configObject):
    ################################################################
    # SET MODELS. OVERRIDES CONFIGURATION FROM FILE
    from project_configuration.dataset import dataset_sim
    configObject.args.engine.dataset = dataset_sim.MyDataset(configObject)


def override_signal(configObject):
    from project_configuration.models import signal_model_fastMRI
    configObject.args.engine.signal_model = signal_model_fastMRI.Signal_model(configObject)


def override_rim(configObject):
    from project_configuration.inference_model.rim import rim_modified
    configObject.args.engine.inference_model = rim_modified.Rim(configObject)


def override_likelihood(configObject):
    from project_configuration.models.likelihood_model.gaussian import gaussian
    configObject.args.engine.likelihood_model = gaussian.Gaussian(configObject)


def evaluate_processed_data(processed_data, epoch, exper_name):
    f = h5py.File('data/evaluation/' + exper_name + '.h5', 'a')
    SSIM_list = []
    PSNR_list = []
    NMSE_list = []
    SSIM_init = []
    PSNR_init = []
    NMSE_init = []
    for subject in processed_data:
        SSIM_list_ = []
        PSNR_list_ = []
        NMSE_list_ = []
        SSIM_init_ = []
        PSNR_init_ = []
        NMSE_init_ = []
        for patch in subject:
            label_eval = converter.real_to_abs(patch['labels'])[0]
            initial_eval = converter.real_to_abs(patch['initial'])[0]
            mask_eval = patch['mask'][0]
            estimate_eval = converter.real_to_abs(patch['estimated'])
            SSIM_list_.append(evaluate_data.ssim(label_eval, estimate_eval, mask_eval))
            PSNR_list_.append(evaluate_data.psnr(label_eval, estimate_eval, mask_eval))
            NMSE_list_.append(evaluate_data.nmse(label_eval, estimate_eval, mask_eval))
            SSIM_init_.append(evaluate_data.ssim(label_eval, initial_eval, mask_eval))
            PSNR_init_.append(evaluate_data.psnr(label_eval, initial_eval, mask_eval))
            NMSE_init_.append(evaluate_data.nmse(label_eval, initial_eval, mask_eval))
        SSIM_list.append(np.stack(SSIM_list_))
        PSNR_list.append(np.stack(PSNR_list_))
        NMSE_list.append(np.stack(NMSE_list_))
        SSIM_init.append(np.stack(SSIM_init_))
        PSNR_init.append(np.stack(PSNR_init_))
        NMSE_init.append(np.stack(NMSE_init_))
    epoch_str = str(epoch)
    if len(epoch_str) == 2:
        gr_name = '00' + epoch_str
    elif len(epoch_str) == 3:
        gr_name = '0' + epoch_str
    else:
        gr_name = epoch_str
    grp = f.create_group(gr_name)
    grp.create_dataset('SSIM', data=np.stack(SSIM_list))
    grp.create_dataset('PSNR', data=np.stack(PSNR_list))
    grp.create_dataset('NMSE', data=np.stack(NMSE_list))
    grp.create_dataset('SSIM_init', data=np.stack(SSIM_init))
    grp.create_dataset('PSNR_init', data=np.stack(PSNR_init))
    grp.create_dataset('NMSE_init', data=np.stack(NMSE_init))
    f.close()


if __name__ == '__main__':
    model = 'Fv024'
    data = 'fastMRI'
    experiment_name = 'trained_' + model + '_infer_fastMRI_multicoil_val'
    f = h5py.File('data/evaluation/' + experiment_name + '.h5', 'w')
    f.close()
    # epochs = [25, 50, 100, 200, 400, 600, 800, 1000]
    epochs = [20, 50, 100, 200, 350]
    # epochs = [10, 40, 80, 120, 160, 220]
    for epoch in epochs:
        configurationObj = pll_configuration.Configuration('TESTING')
        configurationObj.sensitivityMap = 0
        configurationObj.args.engine.batchSize = 1
        configurationObj.args.engine.epochs = 1
        configurationObj.args.task.numberOfPatches = 10
        configurationObj.args.task.useRandomSeed = False
        configurationObj.args.engine.saveResultsPath = 'data/generated/estimated_data/'
        configurationObj.args.engine.loadCheckpoint = True
        configurationObj.args.engine.suffixCheckpoint = model + '_epoch' + str(epoch) + '_' + data
        if model[0] == 'F':
            configurationObj.args.engine.loadCheckpointPath = 'trained_models/fastMRI/' + model + 'rim_epoch_' + str(
                epoch) + '.pth'
        else:
            configurationObj.args.engine.loadCheckpointPath = 'trained_models/sim/' + model + 'rim_epoch_' + str(
                epoch) + '.pth'
        configurationObj.args.engine.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        override_model(configurationObj)
        override_signal(configurationObj)
        override_rim(configurationObj)
        override_likelihood(configurationObj)
        configObject = core_build.make(configurationObj)
        if configurationObj.args.engine.lossFunction == 'NMSE':
            configObject.args.engine.objective_fun = NMSE.NMSE
        elif configurationObj.args.engine.lossFunction == 'SSIM':
            configObject.args.engine.objective_fun = SSIM.SSIM
        logging.info('{} model succesfully built.'.format(configurationObj.args.engine.inference_model.__name__))
        logging.info('Starting testing....')
        infer_model = estimate.Infer(configObject)
        processed_data = infer_model.execute(tr_epoch=str(epoch), return_result=True)
        if configObject.args.engine.evaluateResults:
            evaluate_processed_data(processed_data, str(epoch), experiment_name)
        del configurationObj, processed_data
