import h5py
import numpy as np
import matplotlib.pyplot as plt
import xml.etree.ElementTree as etree
import torch
import torch.fft
from project_configuration.utilities import fourier_transforms as fft
from project_configuration.utilities import transforms
from project_configuration.utilities.fast_mri_data import et_query

# result2 = h5py.File('D:/sens_maps/file_brain_AXT1_202_2020438.h5', 'r+')
result = h5py.File('C:/Users/milan/OneDrive/Documenten/Thesis/data/fastMRI/multicoil_val/file_brain_AXT1_202_2120022.h5')
# result = h5py.File('C:/Users/milan/OneDrive/Documenten/Thesis/data/fastMRI/multicoil_train/file_brain_AXT1_201_6002725.h5')

# result = h5py.File('data/fastMRI/file_brain_AXFLAIR_200_6002441.h5')
print(list(result.keys()))
kspace = result['kspace']
slice = kspace[0, 8]
lim = 0.00002
# plt.imshow(slice.real, vmin=-lim/2, vmax=lim)
# plt.colorbar()
# plt.title('k-space of fastMRI data')
# plt.show()
# sens_maps = result2['reconstruction'][()]
# plt.imshow(sens_maps[1,0,..., 0].real)
# plt.show()



# image = fft.ifft2c_new(torch.view_as_real(torch.from_numpy(slice)))
rss = result['reconstruction_rss'][1]
et_root = etree.fromstring(result["ismrmrd_header"][()])
enc = ["encoding", "encodedSpace", "matrixSize"]
crop_size = (
    int(et_query(et_root, enc + ["x"])),
    int(et_query(et_root, enc + ["y"])),
)
crop_size = (250, 250)
rss_crop = transforms.center_crop(rss, crop_size)
# image_mag = abs(torch.view_as_complex(image_crop))
# plt.imshow(image_mag, cmap='gray')
# plt.colorbar()
# plt.show()
plt.imshow(rss, cmap='gray')
plt.colorbar()
plt.title('RSS of fastMRI data')
plt.show()
