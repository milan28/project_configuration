import matplotlib.pyplot as plt
import numpy as np
import h5py
import os
import matplotlib as mpl
import torch
import math
import matplotlib.ticker as tck

def set_box_color(bp, color):
    plt.setp(bp['boxes'], color=color)
    plt.setp(bp['whiskers'], color=color)
    plt.setp(bp['caps'], color=color)
    plt.setp(bp['medians'], color=color)

def load_folder(fileName, path):
    # mpl.rc('font', family='CMU Sans Serif')
    mpl.rc('ytick', labelsize=15)
    mpl.rc('axes', labelsize=15)
    patch = 0
    list_metrics = {'NMSE' : {},'PSNR' : {}, 'SSIM' : {}}
    metrics_2 = ['NMSE', 'PSNR', 'SSIM']
    label2 = ['lr: 4e-3', 'lr: 2e-3', 'lr: 1e-3', 'lr: 5e-4']
    lab = 0
    save = False
    mag = True
    phase = False
    diff = False
    cmap = plt.get_cmap('PuOr')
    for file in fileName:
        experiment = 'baseline fastMRI vs baseline sim vs final model'
        data_ = h5py.File(os.path.join(path, file), 'r')
        RIM = data_['rimtesting']
        mask = RIM['mask']
        estimated_real_imag = np.moveaxis(RIM['estimated'], -1, 0)
        estimated_abs = np.sqrt(estimated_real_imag[0] ** 2 + estimated_real_imag[1] ** 2)
        label_real_imag = np.moveaxis(RIM['labels'][patch], -1, 0)
        label_abs = np.sqrt(label_real_imag[0] ** 2 + label_real_imag[1] ** 2)
        initial_guess_real_imag = np.moveaxis(RIM['initial'][patch], -1, 0)
        initial_guess_abs = np.sqrt(initial_guess_real_imag[0] ** 2 + initial_guess_real_imag[1] ** 2)
        vmax = label_abs.max()
        # phase = torch.view_as_complex(torch.from_numpy(RIM['estimated'][:])).angle()
        phase_recon = torch.view_as_complex(torch.from_numpy(RIM['estimated'][:])).angle()*mask[0]
        phase_label = torch.view_as_complex(torch.from_numpy(RIM['labels'][patch])).angle()*mask[0]
        vmin = 0
        # plt.figure(figsize=(10,10))
        # plt.imshow(label_abs, cmap='gray', vmax=vmax, vmin=vmin)
        # plt.colorbar()
        # plt.axis('off')
        # if save:
        #     plt.savefig(file +'label.png', bbox_inches='tight', pad_inches=0)
        # plt.title('label')
        # plt.show()
        if phase:
            fig, ax = plt.subplots(figsize=(5,5))
            cax = ax.imshow(phase_label)
            # Add colorbar, make sure to specify tick locations to match desired ticklabels
            cbar = fig.colorbar(cax, ticks=[-math.pi+0.1,-math.pi/2, 0 ,math.pi/2, math.pi-0.1])
            cbar.ax.set_yticklabels(['-$\pi$','-$\pi$/2', '0','$\pi$/2', '$\pi$'])  # vertically oriented colorbar
            plt.axis('off')
            if save:
                plt.savefig(file + 'phase_label_colorbar.png', bbox_inches='tight', pad_inches=0)
            plt.show()
            plt.figure(figsize=(10,10))
            plt.imshow(phase_label)
            plt.axis('off')
            if save:
                plt.savefig(file + 'phase_label.png', bbox_inches='tight', pad_inches=0)
            plt.title('phase label')
            plt.show()
            plt.imshow(phase_recon)
            plt.axis('off')
            if save:
                plt.savefig(file + 'phase_estimated.png', bbox_inches='tight', pad_inches=0)
            plt.title('phase estimated')
            plt.show()
            plt.show()
            fig1, ax1 = plt.subplots(figsize=(5, 5))
            diff_phase = ((phase_recon-phase_label)+ math.pi) % (2*math.pi) - math.pi
            bax = plt.imshow(diff_phase,cmap=cmap, vmax=math.pi, vmin=-math.pi)
            plt.axis('off')
            bbar = fig1.colorbar(bax, ticks=[-math.pi + 0.1, -math.pi / 2, 0, math.pi / 2, math.pi - 0.1])
            bbar.ax.set_yticklabels(['-$\pi$', '-$\pi$/2', '0', '$\pi$/2', '$\pi$'])
            if save:
                plt.savefig(file + 'phase_diff_colorabar.png', bbox_inches='tight', pad_inches=0)
            plt.title('phase diff')
            plt.show()

        if mag:
            plt.imshow(estimated_abs, cmap='gray', vmax=vmax, vmin=vmin)
            plt.axis('off')
            plt.colorbar()
            if save:
                plt.savefig(file + 'label.png', bbox_inches='tight', pad_inches=0)
            plt.title('RIM')
            plt.show()
            plt.imshow(estimated_abs, cmap='gray', vmax=vmax, vmin=vmin)
            plt.axis('off')
            if save:
                plt.savefig(file + 'RIM.png', bbox_inches='tight', pad_inches=0)
            plt.title('RIM')
            plt.show()
            plt.imshow(initial_guess_abs, cmap='gray', vmax=vmax, vmin=vmin)
            plt.axis('off')
            if save:
                plt.savefig(file + 'init.png', bbox_inches='tight', pad_inches=0)
            plt.title('init')
            plt.show()
        if diff:
            vmax = 0.061540075
            plt.imshow(label_abs-estimated_abs, cmap=cmap, vmax=vmax, vmin=-vmax)
            plt.axis('off')
            plt.colorbar()
            if save:
                plt.savefig(file + 'diff.png', bbox_inches='tight', pad_inches=0)
            plt.title('diff')
            diff_max = abs(label_abs-estimated_abs).max()
            plt.show()









def index_files(path):
    extension = ".h5"
    fileName = sorted(
        [f for f in os.listdir(path) if (os.path.isfile(os.path.join(path, f)) and (extension in f))])
    load_folder(fileName, path)


path = 'C:/Users/milan/OneDrive/Documenten/Thesis/EMC_qMRI_reconstruction/project_configuration/data/generated/estimated_data/baseline'

index_files(path)



