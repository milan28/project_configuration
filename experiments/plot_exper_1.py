import matplotlib.pyplot as plt
import numpy as np
import h5py
import matplotlib as mpl
def set_box_color(bp, color):
    plt.setp(bp['boxes'], color=color)
    plt.setp(bp['whiskers'], color=color)
    plt.setp(bp['caps'], color=color)
    plt.setp(bp['medians'], color=color)

experiment = '4x acceleration'
f = h5py.File('data/evaluation/experiment_1.h5', 'r')
accel = f.keys()
# mpl.rc('font', family='Futura Lt BT')
data = f[list(accel)[0]]
metrics = data.keys()
ticks = range(1, len(accel)+2)
epoch_name = []
log = [1,0,0]
colors = [ '#f03b20','gold', '#3182bd', '#31a354', '#bf21a5', '#ffa500']
for x in range(3):
    bpr = plt.figure(figsize=(2,3))
    for i in range(2):
        type = 1-i
        da = []
        eval_list = []
        metric = list(metrics)[x*2+type]
        for acc in accel:
            da.append(f[acc][metric][:].flatten())

        bpr = plt.boxplot(da, positions=np.array(range(0,2,1))-type*0.2)
        set_box_color(bpr, colors[type])
    # plt.xticks(ticks, ['zero filled'] + epoch_name)
    plt.title(metric)
    if log[x]:
        plt.yscale('log')
    plt.xticks([-0.1, 0.9],['4x', '8x'])
    plt.xlabel('Acceleration')
    plt.tight_layout()
    plt.show()

