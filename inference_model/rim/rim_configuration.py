from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.insert(0, '../')
import os

import json
import logging
from EMCqMRI.core.utilities import configuration_utilities


class Configuration(object):
    def __init__(self, configObject):
        self.configObject = configObject
        self.required = False

    def parse_configuration(self):       
        group_task = self.configObject.parser.add_argument_group('method_config')
        group_task.add_argument('-inputChannels', required=self.required, type=int, help='Number of input channels')
        group_task.add_argument('-outputChannelsLayer1', required=self.required, type=int, help='Number of output channels in layer 1')
        group_task.add_argument('-outputChannelsLayer2', required=self.required, type=int, help='Number of output channels in layer 2')
        group_task.add_argument('-outputChannelsLayer3', required=self.required, type=int, help='Number of output channels in layer 3')
        group_task.add_argument('-outputChannels', required=self.required, type=int, help='Number of output channels')
        group_task.add_argument('-inferenceSteps', required=self.required, type=int, help='Number of inference steps')

        config_args, _ = self.configObject.parser.parse_known_args()
        configuration = configuration_utilities.convert_argparse_to_attr(config_args)
        return configuration
