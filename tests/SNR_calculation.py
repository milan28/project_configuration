import h5py
import numpy as np
import matplotlib.pyplot as plt
result = h5py.File('C:/Users/milan/OneDrive/Documenten/Thesis/data/fastMRI/multicoil_val/file_brain_AXT1_202_2120022.h5')
print(list(result.keys()))
rss = result['reconstruction_rss']
total_SNR = []
for slice in rss:
    patch_size = 50
    air_patches = []
    air_patches.append(slice[0:patch_size, 0:patch_size])
    air_patches.append(slice[-(patch_size+1):-1, 0:patch_size])
    air_patches.append(slice[0:patch_size, -(patch_size+1):-1])
    air_patches.append(slice[-(patch_size+1):-1, -(patch_size+1):-1])
    patches = np.stack(air_patches)
    max_noise = patches.max()*1.1
    mask = np.ones(slice.shape)
    mask[slice<max_noise] = 0
    x = mask ==1
    S = slice[x].mean()
    N = np.std(patches)
    # N =patches.mean(0)
    SNR = S/N
    total_SNR.append(SNR)
    print(SNR)
total = np.array(total_SNR)
max_SNR = total.max()
min_SNR = total.min()
plt.imshow(slice)
plt.colorbar()
plt.show()
plt.imshow(mask)
plt.show()
