import pandas as pd
import matplotlib.pyplot as plt
# files = ["2", "4", "8"]
# labels = ["acceleration: 2x", "acceleration: 4x", "acceleration: 8x"]
files = ["MSE"]
# labels = ["channels: 40", "channels: 60", "channels: 80", "channels: 100"]
for i in range(len(files)):
    f = pd.read_csv(r"data/tensorboard/" + files[i] + ".csv")
    fx = pd.DataFrame(f, columns=['Step'])
    fy = pd.DataFrame(f, columns=['Value'])
    # plt.plot(fx, fy, label=labels[i])
    plt.plot(fx, fy)
# f = pd.read_csv(r"data/tensorboard/stable.csv")
# fx = pd.DataFrame(f, columns=['Step'])
# fy = pd.DataFrame(f, columns=['Value'])
# plt.plot(fx, fy)
plt.xlabel("epochs")
plt.ylabel("loss")
plt.title("MSE loss curve \n nr. patches: 100, batch size: 10, \n  learning rate: 5e-4, nr. channels 100, \n acceleration factor: 2x" )
# plt.title("Loss curve for varying amount of channels" "\n" "undersampling density: 0.5, learning rate: e-4")
plt.xlim(0, 3000)
plt.yscale("log")
plt.legend()
plt.grid(True, which="both")
plt.show()

