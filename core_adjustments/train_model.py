from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys

sys.path.insert(0, '../')

from EMCqMRI.core.utilities import image_utilities
from EMCqMRI.core.utilities import core_utilities
from EMCqMRI.core.utilities import checkpoint_utilities
import logging
import numpy as np
import matplotlib.pyplot as plt
import torch
# from ray import tune
from torch.utils.tensorboard import SummaryWriter
from project_configuration.utilities import converter
from project_configuration.utilities import evaluate_data
from skimage.metrics import structural_similarity as ssim2

logging.basicConfig(level=logging.INFO)
logging.info('Setting up environment...')


class Train(object):
    """Performs a forward pass through a given inference model.
        Depending on the options set, it might save intermediate results and training checkpoints
        and/or display the intermediate results.

    Args:
        configObject ([type: Configuration]): [Object containing all backend configuration settings]
        Required configObject.args:
            - epochs
            - inference_model
            - dataloader
            - signal_model (if configObject.args.inference_model.__require_initial_guess__ == True)
            - numberOfPatches
            - objective_fun
            - optimizer
            - saveResults
            - saveResultsPath
            - saveCheckpoint
            - saveCheckpointPath
            - usePatchesAsBatches
    """

    def __init__(self, configObject):
        self.args = configObject.args

    def execute(self, viz_signal=False, viz_label=False, viz_estimated=False):
        writer = SummaryWriter()
        if isinstance(self.args.engine.dataloader, list):
            dataloader_state = core_utilities.alternateTrainingState(self.args.engine.dataloader,
                                                                     self.args.engine.batchSize)
            state = next(dataloader_state)
            self.dataloader = list(state.values())[0]
            self.args.engine.stateName = list(state.keys())[0]
        else:
            self.args.engine.stateName = 'training'
            self.dataloader = self.args.engine.dataloader

        epoch = 0
        av_loss_list = []
        for _ in range(self.args.engine.epochs):
            total_loss = 0
            NMSE_total = 0
            SSIM_total = 0
            PSNR_total = 0
            nr_patches = 0
            logging.info('Running {}'.format(self.args.engine.stateName))
            logging.info('*' * 50)
            self.args.engine.lenDataset = len(self.dataloader)
            for i, data in enumerate(self.dataloader):
                self.args.engine.iter = i
                if self.args.engine.usePatchesAsBatches:
                    batch_generator = core_utilities.batch_iterator(self.args.task.numberOfPatches,
                                                                    self.args.engine.batchSize)
                    patch_iterations = int(np.ceil(self.args.task.numberOfPatches / self.args.engine.batchSize))
                    for _ in range(patch_iterations):
                        next(batch_generator)
                        batched_data, indexes = core_utilities.get_batch(data, self.args, batch_generator)
                        signal = batched_data[0]

                        if viz_signal:
                            image_utilities.imagebrowse_slider(signal[0])

                        if len(batched_data) > 1:
                            label = batched_data[1]
                            mask = batched_data[2]
                            data_ = [signal, label, mask]

                            if viz_label:
                                image_utilities.imagebrowse_slider(label[0])

                        else:
                            data_ = [signal]
                        processed_data, loss = self.__run__(data_, indexes)
                        processed_data['kspace'] = data_[0].cpu().detach().numpy()
                        for est, lab, mask in zip(processed_data['estimated'], processed_data['labels'], processed_data['mask']):
                            NMSE_total += evaluate_data.nmse(converter.real_to_abs(lab), converter.real_to_abs(est), mask)
                            SSIM_total += evaluate_data.ssim(converter.real_to_abs(lab), converter.real_to_abs(est), mask)
                            PSNR_total += evaluate_data.psnr(converter.real_to_abs(lab), converter.real_to_abs(est), mask)
                            nr_patches += 1
                        self.__log_error__(epoch, i, indexes, loss)
                        total_loss += loss.cpu().detach()
                        if viz_estimated and indexes[0] % 40 == 0:
                            image_utilities.imagebrowse_slider(processed_data['estimated'])
                else:
                    processed_data, loss = self.__run__(data)
                    self.__log_error__(epoch, i, 1, loss)
                    total_loss += loss.cpu().detach()
                    total

            if self.args.engine.saveResults and epoch % self.args.engine.checkpointBatch == 0:
                image_utilities.saveItermediateResults(processed_data, self.args, epoch)

            if self.args.engine.stateName == 'training':
                av_loss = total_loss /  nr_patches
                av_NMSE = NMSE_total /  nr_patches
                av_SSIM = SSIM_total /  nr_patches
                av_PSNR = PSNR_total / nr_patches
                writer.add_scalar('training loss', av_loss, epoch)
                writer.add_scalar('SSIM', av_SSIM, epoch)
                writer.add_scalar('NMSE', av_NMSE, epoch)
                writer.add_scalar('PSNR', av_PSNR, epoch)
                writer.add_image('initial guess', abs(converter.real_to_complex(processed_data['initial'])[0]), epoch,
                                 dataformats='HW')
                writer.add_image('estimated', abs(converter.real_to_complex(processed_data['estimated'])[0]), epoch,
                                 dataformats='HW')
                writer.add_image('label', abs(converter.real_to_complex(processed_data['labels'])[0]), epoch,
                                 dataformats='HW')
                writer.add_image('mask', processed_data['mask'][0], epoch, dataformats='HW')
                epoch += 1
                if self.args.engine.saveCheckpoint and epoch % self.args.engine.checkpointBatch == 0:
                    checkpoint_utilities.save(self.args, epoch, self.args.engine.inference_model)

            if isinstance(self.args.engine.dataloader, list):
                state = next(dataloader_state)
                self.dataloader = list(state.values())[0]
                self.args.engine.stateName = list(state.keys())[0]
                self.args.engine.inference_model.train() if self.args.engine.stateName == 'training' else self.args.engine.inference_model.eval()

    def __log_error__(self, epoch, i, indexes, loss):
        logging.info("Epoch: {}, State: {}, Subject: {}/{}, Patches {}/{}, Loss: {} ".format(epoch,
                                                                                             self.args.engine.stateName,
                                                                                             i,
                                                                                             self.dataloader.__len__(),
                                                                                             indexes[0],
                                                                                             self.args.task.numberOfPatches,
                                                                                             loss))

    def __run__(self, data_, indexes):
        nr_patches = indexes[0] - indexes[1]
        signal = data_[0]
        if len(data_) > 1:
            label = data_[1]
            mask = data_[2]

        if self.args.engine.inference_model.__require_initial_guess__:
            initial_w_img, coil_img = self.args.engine.signal_model.initializeParameters(signal, indexes)
            inputs = [signal, initial_w_img]
            # inputs = [signal, label]
        else:
            inputs = [signal]

        estimate = self.args.engine.inference_model.forward(inputs, indexes)
        loss_batch = 0
        if isinstance(estimate, list):
            T = len(estimate)
            loss = []
            for timestep, e in enumerate(estimate):
                loss.append(self.args.engine.objective_fun(e, label, timestep + 1, T))
            loss = sum(loss) / len(loss) / nr_patches
        else:
            loss = self.args.engine.objective_fun(estimate, label)

        if self.args.engine.stateName == 'training':
            loss.backward()
            self.args.engine.optimizer.step()
            self.args.engine.optimizer.zero_grad()

        last_estimate = estimate[-1]
        processed_data = {}
        processed_data['initial'] = initial_w_img.cpu().detach().numpy()
        processed_data['estimated'] = last_estimate.cpu().detach().numpy()
        processed_data['coil_images'] = coil_img.detach().numpy()
        if len(data_) > 1:
            processed_data['labels'] = label.cpu().detach().numpy()
            processed_data['mask'] = mask.cpu().detach().numpy()

        return processed_data, loss
