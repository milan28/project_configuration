import h5py
import numpy as np
import matplotlib.pyplot as plt
import math
import torch
coils_img_1 = torch.view_as_complex(self.transform_k_space_to_image(k_space))

slice = 0
coil = 10
coils_img = coils_img_1[slice, coil]
result = h5py.File('D:/sen_maks_maps/file_brain_AXT1_201_6002725.h5')
sen_maks_all = result['reconstruction']
sen_maks = sen_maks_all[slice][0, :, :, coil]
sen_maks[sen_maks.real == 0] =np.nan + np.nan*1j
sen_maks[sen_maks.imag == 0] =np.nan + np.nan*1j
phase = np.arctan(sen_maks.imag /sen_maks.real)
multi = np.divide(coils_img, sen_maks)
# vmax= np.nanmax(abs(multi))
# vmin = np.nanmin(abs(multi))
# plt.imshow(abs(coils_img))
# plt.title('coil image')
# plt.colorbar()
# plt.show()
plt.imshow(abs(sen_maks))
plt.title('sen_maksitivity mask magnitude')
plt.colorbar()
plt.show()
plt.imshow(phase)
plt.title('sen_maksitivity mask phase')
plt.colorbar()
plt.show()
factor = 14
# plt.imshow(abs(multi), vmax=vmax/factor, vmin=vmin/factor)
# plt.colorbar()
# plt.title('coil image divided by sen_maksitivity')
# plt.show()

#
# test = torch.from_numpy(sen_maks_mask)
# img = torch.view_as_complex(test[7]).numpy()
# plt.imshow(abs(img))
# plt.show()

# test = abs(torch.view_as_complex(training_label_multicoil[0,2])).numpy()
# plt.imshow(test, cmpa= 'gray')
# plt.show()
#
# plt.imshow(abs(torch.view_as_complex(training_label[0])), vmax =1.5)
# plt.colorbar()
# plt.show()
#
#
# plt.imshow(abs(coil_sen_maks.detach().numpy()))
# plt.show()
# plt.imshow(abs(coil_img.detach().numpy()))
# plt.show()
# plt.imshow(abs(self.test.detach().numpy()))
# plt.show()
#
# test = torch.view_as_complex(param_map_gradient)
# plt.imshow(abs(test))
# plt.title('gradient map')
# plt.colorbar()
# plt.show()

import torch
test = initial_w_img[0,...,0]
noise2= test[mask[0]==0]
patch = abs(torch.view_as_complex(initial_w_img[0])).numpy()
plt.imshow(patch, cmap='gray')
plt.show()
mean = patch[mask[0] == 1].mean()
patch_only_noise = patch[mask[0] == 0]
std = np.std(patch_only_noise)
SNR = mean/std


coil_img_com = torch.view_as_complex(coil_img)
coil_img_com = coil_img.detach()
plt.subplot(2,2,1)
plt.imshow(coil_img_com.real)
plt.colorbar()
plt.title('real')
plt.subplot(2,2,2)
plt.imshow(coil_img_com.imag)
plt.colorbar()
plt.title('imag')
plt.subplot(2,2,3)
plt.imshow(abs(coil_img_com))
plt.colorbar()
plt.title('abs')
plt.show()


coil = 0
slice = 0
coil_img_1 = torch.view_as_complex(self.coil_img[slice,coil])*3000
coil_img_2 = torch.view_as_complex(img_sens).detach()
phase = torch.arctan(coil_img_1.imag/coil_img_1.real)
phase_es = torch.arctan(coil_img_2.imag/coil_img_2.real)
m=0.08
plt.subplot(1,2,1)
plt.imshow(phase)
plt.title('phase of original coil image')
plt.colorbar()
plt.subplot(1,2,2)
plt.imshow(phase_es)
plt.title('phase of estimated coil image')
plt.colorbar()
plt.show()
plt.subplot(1,2,1)
plt.imshow(coil_img_1.real, vmax = m, vmin= -m)
plt.title('origianl coil image real')
plt.colorbar()
plt.subplot(1,2,2)
plt.imshow(coil_img_2.real, vmax = m, vmin= -m)
plt.title('estimated coil image real')
plt.colorbar()
plt.show()
plt.subplot(1,2,1)
plt.imshow(coil_img_1.imag, vmax = m, vmin= -m)
plt.title('origianl coil image imaginary')
plt.colorbar()
plt.subplot(1,2,2)
plt.imshow(coil_img_2.imag, vmax = m, vmin= -m)
plt.title('estimated coil image imaginary')
plt.colorbar()
plt.show()
plt.subplot(1,2,1)
plt.imshow(abs(coil_img_1), vmax = m, vmin= -m)
plt.title('original coil image magnitude')
plt.colorbar()
plt.subplot(1,2,2)
plt.imshow(abs(coil_img_2), vmax = m, vmin= -m)
plt.title('estimated coil image magnitude')
plt.colorbar()
plt.show()


img_1 = abs(torch.view_as_complex(self.test[0]))
img_2 = abs(torch.view_as_complex(label_guess[0]))
plt.subplot(1,2,1)
plt.imshow(img_1)
plt.axis('off')
plt.subplot(1,2,2)
plt.imshow(img_2)
plt.axis('off')
plt.show()

self.args.task.kCenter_fractions = 0.08
self.args.task.kDensity = 0.25

self.args.task.undMask = self.create_undersampling_mask(img_size, 0.08,
                                                                    0.25)
self.args.task.kCenter_fractions = 0.04
self.args.task.kDensity = 0.125
self.args.task.undMask2 = self.create_undersampling_mask(img_size, 0.04,
                                                                    0.125)
img_1 = self.args.task.undMask
img_2 = self.args.task.undMask2
plt.subplot(1,2,1)
plt.imshow(img_1, cmap='gray')
plt.axis('off')
plt.subplot(1,2,2)
plt.imshow(img_2, cmap='gray')
plt.axis('off')
plt.show()


import xml.etree.ElementTree as etree
from project_configuration.utilities.fast_mri_data import et_query
TI = []
TE = []
TR = []
for file in self.fileName:
    with open(os.path.join(self.path, file), 'rb') as f:
        data_ = h5py.File(f, 'r')
        et_root = etree.fromstring(data_["ismrmrd_header"][()])
        enc = ["TI"]
        TI.append(float(et_query(et_root, enc)))
        enc = ["TE"]
        TE.append(float(et_query(et_root, enc)))
        enc = ["TR"]
        TR.append(float(et_query(et_root, enc)))

h5f = h5py.File('scanning_parameters.h5', 'w')
h5f.create_dataset('TI', data=np.array(TI))
h5f.create_dataset('TE', data=np.array(TE))
h5f.create_dataset('TR', data=np.array(TR))

h5f.close()

table = zip(TI, TR, TE)

plt.subplot(2,2,1)
plt.imshow(torch.abs(kappa[0])*(1-term_1+term_2))
plt.title('first part')
plt.colorbar()
plt.subplot(2,2,2)
plt.imshow(1-term_1)
plt.title('term_1')
plt.colorbar()
plt.subplot(2,2,3)
plt.imshow(term_2)
plt.title('term_2')
plt.colorbar()
plt.subplot(2,2,4)
plt.imshow(term_3)
plt.title('term_3')
plt.colorbar()
plt.show()



plt.subplot(2,2,1)
plt.imshow((torch.abs(kappa[0])*(1-term_2)*term_3)[0], cmap = 'gray')
plt.title('T1 weighted')
plt.colorbar()
plt.subplot(2,2,2)
plt.imshow((torch.abs(kappa[0]) * (1 -2*term_1+term_2) * term_3)[0], cmap = 'gray')
plt.title('inversion recovery')
plt.colorbar()
plt.subplot(2,2,3)
plt.imshow(rss, cmap = 'gray')
plt.title('RSS of fastMRI data')
plt.colorbar()
plt.subplot(2,2,4)
plt.imshow(abs((torch.abs(kappa[0]) * (1 -2*term_1+term_2) * term_3)[0]), cmap = 'gray')
plt.title('abs img of inversion recovery')
plt.colorbar()
plt.show()

slice = 0
coil = 1
coil_img_0 = center_crop(torch.view_as_complex(coils_img[slice, coil]), (320,320))
rss_0 = training_label[slice]
plt.subplot(2,2,1)
plt.imshow(rss_0.abs())
plt.title('rss magnitude')
plt.colorbar()
plt.subplot(2,2,2)
plt.imshow(rss_0.angle())
plt.title('rss phase')
plt.colorbar()
plt.subplot(2,2,3)
plt.imshow(coil_img_0.abs())
plt.title('coil magnitude')
plt.colorbar()
plt.subplot(2,2,4)
plt.imshow(coil_img_0.angle())
plt.title('coil phase')
plt.colorbar()
plt.tight_layout()
plt.show()

slice = 0
coil = 0
sens = center_crop(self.sens_map[self.slices[slice], coil],(320,320)).numpy()
inv = np.linalg.pinv(sens, rcond=1e-5)
img = center_crop(torch.view_as_complex(coils_img[slice, coil]), (320,320)).numpy()
test2 = np.dot(sens, np.dot(inv,sens))
test = np.dot(img,sens)
# vmax =  0.0001
plt.imshow(abs(test))
plt.colorbar()
plt.show()
plt.imshow(abs(test2))
plt.colorbar()
plt.show()


slice = 0
coil = 0
sens = center_crop(self.sens_map[self.slices[slice]],(320,320))
img = center_crop(torch.view_as_complex(coils_img[slice]), (320,320))
coil_combined = torch.zeros((320,320), dtype=torch.cfloat)
for sens_, img_ in zip(sens, img):
    coil_combined += img_ * torch.conj(sens_)


slice = 0
coil = 0
GT = torch.view_as_complex(training_label[slice])
sens = center_crop(self.sens_map[self.slices[slice]], (320,320))
img = center_crop(torch.view_as_complex(coils_img[slice]), (320,320))
plt.subplot(1,2,1)
plt.imshow(abs(GT))
plt.colorbar()
plt.title('magnitude')
plt.subplot(1,2,2)
plt.imshow(GT.angle())
plt.colorbar()
plt.title('phase')
plt.show()


coil_img_rec = sens[coil]*GT
plt.subplot(3,2,1)
plt.imshow(coil_img_rec.abs())
plt.colorbar()
plt.title('magnitude recon coil ')
plt.subplot(3,2,2)
plt.imshow(img[coil].abs())
plt.colorbar()
plt.title('magnitude orig coil' )
plt.subplot(3,2,3)
plt.imshow(coil_img_rec.angle())
plt.colorbar()
plt.title('angle recon coil ')
plt.subplot(3,2,4)
plt.imshow(img[coil].angle())
plt.colorbar()
plt.title('angle orig coil' )
plt.subplot(3,2,5)
plt.imshow(sens[coil].abs())
plt.colorbar()
plt.title('magnitude sens_mask' )
plt.tight_layout()
plt.show()


import xml.etree.ElementTree as etree
from project_configuration.utilities.fast_mri_data import et_query
TE = []
TR = []
for file in self.fileName:
    with open(os.path.join(self.path, file), 'rb') as f:
        data_ = h5py.File(f, 'r')
        et_root = etree.fromstring(data_["ismrmrd_header"][()])
        enc = ["TE"]
        TE.append(float(et_query(et_root, enc)))
        enc = ["TR"]
        TR.append(float(et_query(et_root, enc)))

import xml.etree.ElementTree as etree
from project_configuration.utilities.fast_mri_data import et_query
model =[]
for file in self.fileName:
    with open(os.path.join(self.path, file), 'rb') as f:
        data_ = h5py.File(f, 'r')
        et_root = etree.fromstring(data_["ismrmrd_header"][()])
        enc = ["systemModel"]
        model.append(str(et_query(et_root, enc)))

size = 14
plt.rcParams['axes.titlesize'] = 18
mpl.rc('xtick', labelsize=size)
mpl.rc('ytick', labelsize=size)
mpl.rc('axes', labelsize=size)
mpl.rc('legend', fontsize=size)
plt.figure(figsize=(2, 4))
bpr = plt.boxplot(met_[fileName[0]][0], positions=np.array([0.5]), sym='', widths=0.4)
set_box_color(bpr, 'orange')
bpr = plt.boxplot(met_[fileName[0]][6], positions=np.array([1]), sym='', widths=0.4)
set_box_color(bpr, '#3182bd')
plt.yscale('log')
plt.title('NMSE')
plt.tight_layout()
plt.tick_params(
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom=False,      # ticks along the bottom edge are off
    top=False,         # ticks along the top edge are off
    labelbottom=False)
plt.savefig('4_sim_NMSE.png', bbox_inches='tight',pad_inches = 0.5)
plt.show()



plt.imshow(initial_guess[i], cmap='gray', vmax=vmax, vmin=vmin)
# plt.colorbar()
plt.show()
plt.axis('off')
# plt.savefig('4_sim_init.png', bbox_inches='tight',pad_inches = 0)