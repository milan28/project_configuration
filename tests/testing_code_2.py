import matplotlib.pyplot as plt
import torch
plt.subplot(2,2,1)
plt.imshow(abs(torch.view_as_complex(kappa[0].detach())))
plt.colorbar()
plt.subplot(2,2,2)
plt.title('estimate')
plt.imshow(abs(torch.view_as_complex(dx[0].detach())))
plt.colorbar()
plt.title('difference')
plt.subplot(2,2,3)
plt.imshow(abs(torch.view_as_complex(paramGrad[0].detach())))
plt.colorbar()
plt.title('gradient map')
plt.show()


patch = 0
lab_real_max = abs(label[patch,...,0]).max()
lab_imag_max = abs(label[patch,...,1]).max()
w_real_max = abs(initial_w_img[patch,...,0]).max()
w_imag_max = abs(initial_w_img[patch,...,1]).max()
ver_label = lab_real_max/lab_imag_max
ver_w = w_real_max/w_imag_max
patch = 0
plt.subplot(2,2,1)
plt.title('label')
plt.imshow(label[patch,...,0])
plt.colorbar()
plt.subplot(2,2,2)
plt.title('estimate')
plt.imshow(initial_w_img[patch,...,0])
plt.colorbar()
plt.show()

