import numpy as np
import torch

def real_to_complex(arg):
    if isinstance(arg, (np.ndarray, np.generic)):
        arg = torch.from_numpy(arg)
    if arg.size()[-1] == 2:
        arg = torch.view_as_complex(arg).detach()
    return arg.numpy()

def real_to_abs(arg):
    if isinstance(arg, (np.ndarray, np.generic)):
        arg = torch.from_numpy(arg)
    if arg.size()[-1] == 2:
        arg = torch.view_as_complex(arg).detach()
    return arg.abs().numpy()