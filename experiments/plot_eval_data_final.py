import matplotlib.pyplot as plt
import numpy as np
import h5py
import scipy.stats as st
import matplotlib as mpl
import math

def set_box_color(bp, color):
    plt.setp(bp['boxes'], color=color)
    plt.setp(bp['whiskers'], color=color)
    plt.setp(bp['caps'], color=color)
    plt.setp(bp['medians'], color=color)
# mpl.rc('font', family='Computer Modern')
mpl.rc('xtick', labelsize=15)
mpl.rc('ytick', labelsize=15)
mpl.rc('axes', labelsize=15)
experiment = '8x acceleration'
f = h5py.File('data/evaluation/final_8acceleration.h5', 'r')
f4= h5py.File('data/evaluation/final_4acceleration.h5', 'r')
files = [f4,f]
epochs = list((f.keys()))
epochs = ['Final model','Baseline RIM']
data = f[epochs[0]]
metrics = data.keys()
ticks = range(1, len(epochs)+3)
epoch_name = ['Trained with  \nsimulated data','Trained with \nin-vivo data']
cs8 = h5py.File('data/evaluation/CS_8.h5', 'r')
cs4 = h5py.File('data/evaluation/CS_4.h5', 'r')
cs_list = [cs4,cs8]
cs_keys = ['NMSE', 'PSNR', 'SSIM']
esp = []
log = [1, 0, 0]
av = {}
CI = {}
for x in range(3):
    inc = 0.2
    bpl = plt.figure(figsize=(10,10))
    pos = 0
    for file, cs in zip(files, cs_list):
        eval_list = []
        metric = list(metrics)[x*2]
        init = file[list(epochs)[0]][metric + '_init'][:]
        eval_list.append(init.flatten())
        for epoch in epochs:
            eval = file[epoch][metric][:]
            eval_list.append(eval.flatten())
        eval_list.append(cs[cs_keys[x]][:])

        colors = ['gold','#f03b20',  '#3182bd', '#31a354', '#bf21a5', '#ffa500']
        for eval, col in zip(eval_list, colors):
            bpl = plt.boxplot(eval, positions=[pos])
            av[file.filename+metric+col] = eval.mean()
            significant_digits = 3
            ci = st.t.interval(alpha=0.95, df=len(eval)-1, loc=np.mean(eval), scale=st.sem(eval))
            CI[file.filename+metric+col] =np.round(ci, significant_digits - int(math.floor(math.log10(abs(ci[0])))) - 1)
            pos+=inc
            set_box_color(bpl, col)
        pos +=inc
    plt.xticks([3*inc/2, 3*inc/2+5*inc], ['4x', '8x'])
    plt.title(metric, fontsize=17)
    if log[x]:
        plt.yscale('log')
    # ax.ticklabel_format(axis='y', style='plain')
    # ax.set_xlabel('inference steps')
    plt.xlabel('Acceleration')
    plt.tight_layout()
    plt.show()

