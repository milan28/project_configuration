import torch
import matplotlib.pyplot as plt
import numpy as np


def plotter(*args, slice = 0, coil = 0, title = ''):
    for t, arg in enumerate(args):
        if isinstance(arg, (np.ndarray, np.generic)):
            arg = torch.from_numpy(arg)
        if arg.size()[-1] == 2:
            arg = torch.view_as_complex(arg).detach()
        if len(arg.size()) == 3:
            arg = arg[slice]
        if len(arg.size()) == 4:
            arg = arg[slice, coil]

        fig1, axs = plt.subplots(2, 2, figsize = (15,15))
        fig1.suptitle(title + 'variable ' + str(t))
        im1 = axs[0,0].imshow(arg.abs(), cmap='gray')
        fig1.colorbar(im1, ax=axs[0,0])
        axs[0,0].set_title("Magnitude")
        im2 = axs[0,1].imshow(arg.angle())
        fig1.colorbar(im2, ax=axs[0,1])
        axs[0,1].set_title("Phase")
        im3 = axs[1, 0].imshow(arg.real, cmap='gray')
        fig1.colorbar(im3, ax=axs[1, 0])
        axs[1, 0].set_title("real")
        im4 = axs[1, 1].imshow(arg.imag, cmap='gray')
        fig1.colorbar(im4, ax=axs[1, 1])
        axs[1, 1].set_title("imag")
        plt.tight_layout()
        plt.show()


