import h5py
import numpy as np
import torch
from scipy import ndimage
from skimage.segmentation import flood
from project_configuration.utilities import evaluate_data
from project_configuration.utilities import converter
import os

def generate_mask(slice):
    max_ind_ = np.argmax(slice)
    max_ind = unravel_index(max_ind_, slice.shape)
    max_val = slice.max()
    mask = ndimage.binary_fill_holes(flood(slice, max_ind, tolerance=max_val - 0.025), structure=np.ones((4, 4)))
    return mask
def unravel_index(index, shape):
    out = []
    for dim in reversed(shape):
        out.append(index % dim)
        index = index // dim
    return tuple(reversed(out))

def evaluate_processed_data(reconstructed, labels, masks):
    SSIM_list= []
    PSNR_list= []
    NMSE_list= []
    for rec, label, mask in zip(reconstructed, labels, masks):
        SSIM_list.append(evaluate_data.ssim(label, rec, mask))
        PSNR_list.append(evaluate_data.psnr(label, rec, mask))
        NMSE_list.append(evaluate_data.nmse(label, rec, mask))
    SSIM = np.stack(SSIM_list)
    PSNR = np.stack(PSNR_list)
    NMSE = np.stack(NMSE_list)
    return NMSE, PSNR, SSIM
def load_folder(fileName, path):
    NMSE_list = []
    PSNR_list = []
    SSIM_list = []
    for file in fileName:
        reconst_ = h5py.File('data/generated/CS/8x/' + file, 'r')
        subject = h5py.File(path + file, 'r')
        reconst = abs(reconst_['reconstruction'][0:-6])
        label = subject['reconstruction_rss'][0:-6]
        norm_factor = 0.6 / label.max()
        label = label * norm_factor
        reconst = reconst * norm_factor
        mask_list = []
        for slice in reconst:
            mask_list.append(generate_mask(slice))
        masks = np.stack(mask_list)
        NMSE, PSNR, SSIM = evaluate_processed_data(reconst, label, masks)
        NMSE_list.append(NMSE)
        PSNR_list.append(PSNR)
        SSIM_list.append(SSIM)
    exper_name = 'CS_8'
    f = h5py.File('data/evaluation/' + exper_name + '.h5', 'w')
    f.create_dataset('SSIM', data=np.stack(SSIM_list).flatten())
    f.create_dataset('PSNR', data=np.stack(PSNR_list).flatten())
    f.create_dataset('NMSE', data=np.stack(NMSE_list).flatten())
    f.close()

def index_files(path):
    extension = ".h5"
    fileName = sorted(
        [f for f in os.listdir(path) if (os.path.isfile(os.path.join(path, f)) and (extension in f))])
    data = load_folder(fileName, path)
    return data



path = 'D:/Thesis/multicoil_test_final/'
list_with_excel_data = index_files(path)

