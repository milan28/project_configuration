import h5py
import numpy as np

result1 = h5py.File('D:/sens_map_training/simulated/file_brain_AXT1_201_6002717.h5', 'r')
sens_1 = result1['sens_map'][1]
result2 = h5py.File('D:/sens_map_training/simulated/file_brain_AXT1_201_6002826.h5', 'r')
sens_2= result2['sens_map'][1]
result3 = h5py.File('D:/sens_map_training/simulated/file_brain_AXT1_202_2020256.h5', 'r')
sens_3 = result3['sens_map'][1]
sens_map_total = np.concatenate((result1,result2, result3))