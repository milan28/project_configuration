import numpy as np
from skimage.metrics import structural_similarity as ssim2

def nmse(label, estimated, mask):
    return np.sum(((estimated - label) ** 2), where=mask) / np.sum(label ** 2, where=mask)


def ssim(estimated, label, mask):
    SSIM_full = ssim2(label, estimated, data_range=label.max(), win_size=7, full=True)
    SSIM = np.mean(SSIM_full[1], where=mask)
    return SSIM

def psnr(estimated, label, mask):
    MSE = np.mean(((estimated - label) ** 2), axis=None, where=mask)
    return 10 * np.log10((abs(label).max() ** 2) / MSE)
