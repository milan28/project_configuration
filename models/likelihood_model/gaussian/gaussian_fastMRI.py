from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from EMCqMRI.core.base import base_likelihood_model
import torch
import math
import matplotlib.pyplot as plt
import numpy as np
import logging


class Gaussian(base_likelihood_model.Likelihood):
    """
        Class for the Gaussian PDF.
        Methods:
            - logLikelihood
                inputs: signal (measured signal), mu (simulated signal) and sigma (SD of the noise)
                outputs: data consistency loss
            - applyNoise
                inputs: a signal and sigma
                outputs: Noisy signal corrupted by additive gaussian noise
    """

    def __init__(self, configObject):
        super(Gaussian, self).__init__()
        self.__name__ = 'Gaussian'
        self.args = configObject.args

    def logLikelihood(self, *args):
        signal = args[0]
        predicted_kSpace = args[1]
        if self.args.task.type == 'singlecoil':
            log_likelihood = torch.sum(abs(torch.view_as_complex(predicted_kSpace - signal)) ** 2)
        elif self.args.task.type == 'multicoil':
            log_likelihood = 0
            for predicted_kSpace_coil, signal_coil in zip(predicted_kSpace, signal):
                log_likelihood += torch.sum(abs(torch.view_as_complex(predicted_kSpace_coil - signal_coil)) ** 2)
        if args[5] == 0:
            logging.info('the log-likelihood at inference step ' + str(args[4]) + ' is: ' + str(log_likelihood))
        return log_likelihood

    def applyNoise(self, signal, sigma):
        signal += torch.from_numpy(np.random.normal(0.0, sigma, signal.size())).to(self.args.engine.device)
        return signal

    def gradients(self, configObject, signal, predicted_weighted_image, index, inference_step, batch, sigma=1):
        predicted_kSpace = self.args.engine.signal_model.generateUndKspace(predicted_weighted_image, index)
        loss = self.logLikelihood(signal, predicted_kSpace, sigma, self.args, inference_step, batch)
        loss.backward()
        param_map_gradient = (predicted_weighted_image.grad.view(predicted_weighted_image.size()))
        del predicted_weighted_image
        return param_map_gradient.detach()
