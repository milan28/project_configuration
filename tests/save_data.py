from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import logging
from EMCqMRI.core.configuration.core import pll_configuration
from EMCqMRI.core.engine import build_model as core_build
from EMCqMRI.core.utilities import image_utilities
import numpy as np
import torch


def override_model(configObject):
    ################################################################
    # SET MODELS. OVERRIDES CONFIGURATION FROM FILE
    from project_configuration.dataset import dataset_sim
    configObject.args.engine.dataset = dataset_sim.MyDataset(configObject)

def override_signal(configObject):
    from project_configuration.tests import signal_model_sim_old
    configObject.args.engine.signal_model = signal_model_sim_old.Signal_model(configObject)

def override_rim(configObject):
    from project_configuration.inference_model.rim import rim_modified
    configObject.args.engine.inference_model = rim_modified.Rim(configObject)


def save_data(args, dataloader):
    for i, data in enumerate(dataloader):
        undersampled_k_space = data[0][0]
        undersampled_k_space_real = undersampled_k_space[:, :, 0, ...]
        undersampled_k_space_imag = undersampled_k_space[:, :, 0, ...]
        weighted_images = data[2][0]
        mask = data[3][0]
        for p, (signal_patch_real, signal_patch_imag, label_patch, mask_patch) in enumerate(zip(undersampled_k_space_real, undersampled_k_space_imag, weighted_images, mask)):
            dataPatch = {}
            dataPatch['weighted_series'] = label_patch
            dataPatch['echo_times'] = np.divide(np.array(args.task.tau), 1000)
            dataPatch['undersampled_k_space_real'] = signal_patch_real
            dataPatch['undersampled_k_space_imag'] = signal_patch_imag
            dataPatch['mask'] = mask_patch
            filename = 'Simulated_' + args.task.weighing + '_patch_' + str(p + 1) + '_subject_' + str(
                i + 1)
            print(np.shape(dataPatch['weighted_series']), np.shape(dataPatch['mask']))
            image_utilities.calculate_snr(dataPatch['weighted_series'], dataPatch['mask'], args.task.sigmaNoise)
            image_utilities.imagebrowse_slider(dataPatch['weighted_series'])
            image_utilities.saveDataPickle(dataPatch, args, filename)

        break

if __name__ == '__main__':
    configurationObj = pll_configuration.Configuration('TRAINING')
    override_model(configurationObj)
    override_signal(configurationObj)
    override_rim(configurationObj)
    configurationObj.args.engine.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    configObject = core_build.make(configurationObj)
    logging.info('{} model succesfully built.'.format(configurationObj.args.engine.inference_model.__name__))
    logging.info('Starting training....')
    save_data(configObject.args, configObject.args.engine.dataloader)
