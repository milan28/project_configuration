import matplotlib.pyplot as plt
import numpy as np
import h5py
import os
import matplotlib as mpl

def set_box_color(bp, color):
    plt.setp(bp['boxes'], color=color)
    # bp.set(color='#7570b3', linewidth=2)
    # bp['boxes'].patch.set_facecolor(color)
    plt.setp(bp['whiskers'], color=color)
    plt.setp(bp['caps'], color=color)
    plt.setp(bp['medians'], color=color)

def load_folder(fileName, path):
    # mpl.rc('font', family='CMU Sans Serif')
    plt.rcParams['axes.titlesize'] = 18
    size = 20
    mpl.rc('xtick', labelsize=size)
    mpl.rc('ytick', labelsize=size)
    mpl.rc('axes', labelsize= size)
    mpl.rc('legend', fontsize=size)
    list_metrics = {'NMSE' : {},'PSNR' : {}, 'SSIM' : {}}
    metrics_2 = ['NMSE', 'PSNR', 'SSIM']
    # label2 = ['channels: 120', 'channels: 95', 'channels: 80', 'channels: 60']
    label2 = ['ψ: 4e-3', 'lr: 2e-3', 'lr: 1e-3', 'lr: 5e-4']
    for file in fileName:
        experiment = 'baseline fastMRI vs baseline sim vs final model'
        data_ = h5py.File(os.path.join(path, file), 'r')
        epochs = data_.keys()
        data = data_[list(epochs)[0]]
        metrics = data.keys()
        ticks = range(1, len(epochs) + 2)
        epoch_name = []
        for epoch in epochs:
            if epoch[0:2] == '00':
                epoch_name.append(epoch[2:4])
            elif epoch[0] == '0':
                epoch_name.append(epoch[1:4])
            else:
                epoch_name.append(epoch)
        all_data = []
        for x, metric in enumerate(metrics_2):
            list_metrics[metric][file] = []
            init = data_[list(epochs)[0]][metric + '_init'][:]
            list_metrics[metric][file] = [init.flatten().tolist()]
            for o, epoch in enumerate(epochs):
                eval = data_[epoch][metric][:]
                list_metrics[metric][file].append(eval.flatten().tolist())
    # fig1, axs = plt.subplots(2, 2, figsize=(12, 10))
    # index1 = [0, 1, 0]
    # index2 = [0, 0, 1]
    log = [1, 0, 0]
    skip = 0
    pos = len(epoch_name)-skip
    for met, l in zip(list_metrics, log):
        plt.figure()
        met_ = list_metrics[met]
        nr = len(met_)
        wide = 0
        colors = ['#f03b20', '#3182bd', '#31a354', '#bf21a5', '#ffa500']
        # colors = ['pink', 'lightblue', 'lightgreen', 'gold']
        bpr = plt.boxplot(met_[fileName[0]][0], positions=np.array([2]), sym='', widths=0.6)
        set_box_color(bpr, 'orange')
        for epochs, color, lab in zip(met_, colors, label2):
            bpr = plt.boxplot(met_[epochs][1:pos], positions=np.array(range(pos-1)) * nr + wide+4.5, sym='', widths=0.6)
            set_box_color(bpr, color)
            wide += 0.8
            # plt.plot(c=color, label=lab)
            # plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
        plt.title(met)
        if l:
            plt.yscale('log')

        plt.xlabel('Epochs')
        # plt.ylabel(met)
        plt.tight_layout()
        plt.xticks(range(int(0.5*(nr)), (len(met_[epochs])-skip-1)*nr+int(0.5*(nr)), nr), ['zero filled'] + epoch_name[0:pos-1])
        plt.show()
    data_.close()

    return data


def index_files(path):
    extension = ".h5"
    fileName = sorted(
        [f for f in os.listdir(path) if (os.path.isfile(os.path.join(path, f)) and (extension in f))])
    data = load_folder(fileName, path)
    return data


path = 'C:/Users/milan/OneDrive/Documenten/Thesis/EMC_qMRI_reconstruction/project_configuration/data/evaluation/simulated/'
list_with_excel_data = index_files(path)



