import matplotlib.pyplot as plt
import numpy as np
import h5py
import matplotlib as mpl

# mpl.rc('font', family='Computer Modern')
size = 17
mpl.rc('xtick', labelsize=size)
mpl.rc('ytick', labelsize=size)
mpl.rc('axes', labelsize=size)
experiment = 'Varying inference steps for reconstruction'
f = h5py.File('data/evaluation/inference_steps_4x_v2.h5', 'r')
epochs = list((f.keys()))
# epochs = ['T6', 'T7', 'T8', 'T9', 'T10']
data = f[epochs[0]]
metrics = data.keys()
ticks = range(1, len(epochs)+1)
epoch_name = ['Trained with  \nsimulated data','Trained with \nin-vivo data']
log = [1, 0, 0]
for x in range(3):
    eval_list = []
    metric = list(metrics)[x*2]
    # init = f[list(epochs)[0]][metric + '_init'][:]
    # eval_list.append(init.flatten())
    for epoch in epochs:
        eval = f[epoch][metric][:]
        eval_list.append(eval.flatten())

    # eval_list.append(esp[x])
    fig, ax = plt.subplots()
    ax.boxplot(eval_list)
    plt.xticks(ticks, range(6,11))
    ax.set_title(experiment, fontsize=17)
    if log[x]:
        ax.set_yscale('log')
    # ax.ticklabel_format(axis='y', style='plain')
    # ax.set_xlabel('inference steps')
    ax.set_title(metric, fontsize=size)
    plt.tight_layout()
    plt.show()

