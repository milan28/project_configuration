from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


from EMCqMRI.core.utilities import image_utilities
import logging
import torch
import torch.nn as nn

logging.basicConfig(level=logging.INFO)
logging.info('Setting up environment...')

class Infer(object):
    """Performs a forward pass through a given inference model.
        Depending on the options set, it might save intermediate results and training checkpoints
        and/or display the intermediate results.

    Args:
        configObject ([type: Configuration]): [Object containing all backend configuration settings]
        Required configObject.args:
            - epochs
            - inference_model
            - dataloader
            - signal_model (if configObject.args.inference_model.__require_initial_guess__ == True)
            - numberOfPatches
            - objective_fun
            - optimizer
            - saveResults
            - saveResultsPath
            - saveCheckpoint
            - saveCheckpointPath
            - usePatchesAsBatches
    """
    def __init__(self, configObject):
        self.args = configObject.args

    def execute(self, return_result = False):
        self.args.engine.stateName = 'testing'
        self.dataloader = self.args.engine.dataloader
        self.args.engine.batchSizeIter = 1

        if isinstance(self.args.engine.inference_model, nn.Module):
            self.args.engine.inference_model.eval()

        epoch = '_estimating_'
        logging.info('Running {}'.format(self.args.engine.stateName))
        logging.info('*'*50)
        self.args.engine.lenDataset = len(self.dataloader)
        for i, data in enumerate(self.dataloader):
            self.args.engine.iter = i
            processed_data = self.__run__(data)

            #TODO Include logs of progress and save results per new data in the loop

            if self.args.engine.saveResults:
                self.args.engine.filename = data[1][0]
                image_utilities.saveItermediateResults(processed_data, self.args, epoch+str(i))

        if return_result:
            return processed_data

    def __run__(self, data_):
        index = [1, 0]
        signal = data_[0].squeeze().unsqueeze(0)
        if len(data_) > 2:
            label = data_[2].squeeze().unsqueeze(0)
            mask = data_[3].squeeze().unsqueeze(0)

        if self.args.engine.inference_model.__require_initial_guess__:
            initial_w_img, coil_img = self.args.engine.signal_model.initializeParameters(signal, index)
            inputs = [signal, initial_w_img]
        else:
            inputs = [signal]

        estimate = self.args.engine.inference_model.forward(inputs, index)

        processed_data = {}
        processed_data['initial'] = initial_w_img.cpu().detach().numpy()
        processed_data['estimated'] = estimate.squeeze().cpu().detach().numpy()
        processed_data['coil_images'] = coil_img.detach().numpy()
        if len(data_) > 2:
            processed_data['labels'] = label.cpu().detach().numpy()
            processed_data['mask'] = mask.cpu().detach().numpy()

        return processed_data
